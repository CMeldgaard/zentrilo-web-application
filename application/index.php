<?php
//Start sessions
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Control debug and error messages
define ('DEBUG', TRUE);

//Define localization
setlocale(LC_ALL, 'danish', 'da_DA', 'dan');

// Website URL and path
if (isset($_SERVER['HTTPS']) &&
    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
  	define('PROTOCOL', 'https://');
}
else {
	define('PROTOCOL', 'http://');
}
define('PATH', PROTOCOL.$_SERVER['HTTP_HOST'].'/');
define('WEBSITE_TITLE', 'Zentrilo');

// Directory separator is set up here because separators are different on Linux and Windows operating systems
define('DS', DIRECTORY_SEPARATOR);

//Define application root
define('ROOT', dirname(__FILE__));

//Define core path
define('COREPATH', realpath(__DIR__.'/core').DS);

//Define modules path
define('MODULEPATH', realpath(__DIR__.'/modules').DS);

//Define presenter path
define('PRESENTERPATH', realpath(__DIR__.'/presenter').DS);

//Load in Rollbar service class
require_once(COREPATH.'Rollbar.php');
require_once(COREPATH.'Level.php');
Rollbar::init(array('access_token' => '61e0d223092a46d291f7fd2349137746'));

// Check for the Zentrilo autoloader
if ( ! file_exists(COREPATH.DS.'Autoloader.php'))
{
	die('No autoloader found. Please verify core folder!');
}
// ...and include the autoloader class
require COREPATH.DS.'Autoloader.php';

//Boot the app up
require_once(ROOT . DS . 'core' . DS . 'bootstrap.php');
//Initialize autoloader
$Autoloader = New Autoloader();
//Initialise CSRFGuard library
csrfProtector::init();
$Auth = Autoloader::newLoad('Modules\\Auth');
$Auth->isLoggedIn();

//Initiate application
New Application();

// ... and execute the request
if(isset($_GET['url'])){
    $url = $_GET['url'];
}else{
    $url = "";
}
$route = new Router();
$route->route($url);
