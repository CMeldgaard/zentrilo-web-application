<ul class="contact-person">
<?php
foreach ($contacts as $contact):
?>
<li class="contact-line getContact" data-uuid="<?php echo $contact["personUUID"];?>" data-filter="<?php echo $contact["fName"].' '.$contact["lName"];?>">
  <div class="clearfix uk-grid">
    <div class="avatar uk-width-1-10">
        <img class="img-circle img-responsive pull-left" src="/assets/images/person_avatar.png" alt="">
    </div>
    <div class="uk-width-9-10">
      <span class="name">
          <?php echo $contact["fName"].' '.$contact["lName"];?>
      </span>
      <span class="company"><?php echo $contact["companyName"];?></span>
      <div class="details">
        <?php if($contact["email"]):?>
          <div class="detail">
            E: <span><i class="icon-envelope"></i><a href="mailto:<?php echo $contact["email"];?>"><?php echo $contact["email"];?></a></span>
          </div>
        <?php endif;?>
        <?php if($contact["phone"]):?>
          <div class="detail">
            T: <span><i class="icon-phone"></i><a href="tel:<?php echo $contact["phone"];?>"><?php echo $contact["phone"];?></a></span>
          </div>
        <?php endif;?>
      </div>
    </div>
  </div>
</li>
<?php
endforeach
?>
</ul>
