<div class="header-text">
  <h1><?php echo $contact['fName']; ?> <?php echo $contact['lName']; ?>

    <div data-uk-dropdown="{mode:'click'}">
      <div><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
      <div class="uk-dropdown">
          <ul class="uk-nav uk-nav-dropdown contact-tools">
              <li><a href="#" class="editContact" data-uuid="<?php echo $contact['personUUID']; ?>" data-type="person">Rediger kontakt</a></li>
              <li><a href="#">Sæt som primær kontakt</a></li>
              <li><a href="#">Tilføj projekt</a></li>
              <li><a class="newTask" data-uuid="<?php echo $contact['personUUID']; ?>" data-callback="getContact">Tilføj opgave</a></li>
              <li><a href="#">Esporter som vCard</a></li>
              <li><a href="#" class="delete deleteContact" data-uuid="<?php echo $contact['personUUID']; ?>" data-type="person">Slet kontakt</a></li>
          </ul>
      </div>
    </div>
  </h1>
  <div class="subtitle">
    <?php echo $contact['worktitle']; ?>
    <span class="link getCompany" onclick="getCompany('<?php echo $contact['companyUUID']; ?>','person','<?php echo $contact['personUUID']; ?>')"><?php echo $contact['companyName']; ?></span>
  </div>
  <?php echo $links;?>
</div>
