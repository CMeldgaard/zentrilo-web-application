<?php
namespace Zentrilo\Modules\Contacts;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->data ="";
          $this->accountUUID = $_SESSION['accountUUID'];
  }

//Load data
 public function load($UUID="",$column="personUUID"){
   $this->data = $this->Model->get($UUID,$column);
   return $this;
 }

//Update contactperson
 public function update($data,$UUID){
   //Sanitize data to prevent XSS
   $data = $this->sanitize($data);
   //Build SQL array
   $data = $this->buildArray($data);
   //Create user based on sent data
   $contactID = $this->Model->update($data,$UUID);
 }

//Create new contactperson
 public function create($data){
   //Sanitize data to prevent XSS
   $data = $this->sanitize($data);
   //Generate SQL array
   $sqlData = $this->buildArray($data);
   //Create user based on sent data
   $personUUID = $this->Model->insert($sqlData);
 }

//Delete contactperson
 public function delete($uuid){
   $this->Model->delete($uuid);
 }

//Return the data object as a array
 public function getData(){
    //Get data as Array
   $contact = $this->data;
   //Return the array
   return $contact;
 }

//Return HTML to presenter
 public function fullList(){
   //Get data as Array
   $contacts = $this->data;
   //Bind data to the view
   $this->get_view()->set('contacts',$contacts);
   //Return generated HTML
   return $this->get_view()->moduleRender('contacts/view/contact-line');
 }

//Build array for sql querires
 private function buildArray($data){
   $contactData = Array ("accountUUID" => $this->accountUUID,
            "companyUUID" => $data["companyUUID"],
            "fName" => $data["fName"],
            "lName" => $data["lName"],
            "worktitle" => $data["worktitle"],
            "phone" => $data["phone"],
            "email" => $data["email"],
            "about" => $data["about"],
            "street" => $data["street"],
            "city" => $data["city"],
            "postal" => $data["postal"]
          );
    return $contactData;
 }

//Generate header
 public function getHeader(){
   //Get data as an array
   $contact = $this->data;
   //Bind data to view
   $this->get_view()->set('contact',$contact[0]);
   //Get social links
   $social = Autoloader::newLoad('Modules\\Social');
   $links = $social->getLinks($contact[0]['personUUID'])->build();
   //Bind sociallinks to view
   $this->get_view()->set('links',$links);
   //Return HTML
   return $this->get_view()->moduleRender('contacts/view/header-person');
 }
}
