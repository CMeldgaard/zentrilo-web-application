<?php
namespace Zentrilo\Modules\Contacts;

class Model extends \Zentrilo\Core\Model {

  protected $accountUUID;

  public function __construct() {
      // Load core model functions
      parent::__construct();
      $this->accountUUID = $_SESSION['accountUUID'];
  }
  //SELECT query
  public function get($UUID,$column){
    //Check if specific person thats needed
    if($UUID){
      //Want specific person
      return $this->getContact($UUID,$column);
    }else{
      //Get all connected persons
      return $this->getConnected();
    }
  }

  private function getContact($UUID,$column){
    //Build query
    return $this->coredb
            ->join('contacts_companies','contacts_persons.companyUUID=contacts_companies.companyUUID')
            ->where("contacts_persons.accountUUID",$this->accountUUID)
            ->where("contacts_persons.".$column,$UUID)
            ->orderby("fName","asc")
            ->get("contacts_persons");
  }

  private function getConnected(){
    //Build query
    return $this->coredb
            ->join('contacts_companies','contacts_persons.companyUUID=contacts_companies.companyUUID')
            ->where("contacts_persons.accountUUID",$this->accountUUID)
            ->orderby("fName","asc")
            ->get("contacts_persons");
  }

  protected function getUUID($id){
    //Get UUID from inserted record
    $data = $this->getContact($id,"entityID");
    //Return UUID
    return $data[0]["personUUID"];
  }

  public function insert($data){
    //Inserted data
    $entityID = $this->coredb->insert('contacts_persons', $data);
    //Return UUID
    return $this->getUUID($entityID);
  }

  public function update($data,$UUID){
    //Update specific person
    return $this->coredb
                ->where('personUUID',$UUID)
                ->where('accountUUID',$this->accountUUID)
                ->update ('contacts_persons', $data);
  }

  public function delete($UUID){
    //Delete specific person
    $this->coredb
                ->where('personUUID',$UUID)
                ->where('accountUUID',$this->accountUUID)
                ->delete ('contacts_persons');
  }
}
