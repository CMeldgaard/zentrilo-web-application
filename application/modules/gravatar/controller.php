<?php
namespace Zentrilo\Modules\Gravatar;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->list ="";
          $this->contact ="";
  }

  public function getGravatar($email,$fname){
    $initial = strtolower($fname[0]);
    $grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?s=68&amp;d=https://cdn.auth0.com/avatars/$initial.png?ssl=1&amp;r=pg";
    return $grav_url;
  }

}
