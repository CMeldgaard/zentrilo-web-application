<?php
namespace Zentrilo\Modules\TypeAhead;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
  }

 public function companyTypeAhead($company){
   //Initiate $JSON
   $JSON = "";
   //Lood thru each company to create JSON
   foreach ($company as $company) {
     $JSON = $JSON ."'" .$company["companyName"]."',";
   }
   //Remove last ","
    $JSON = rtrim($JSON, ", ");
  //Return created JSON
   return '['.$JSON.']';
 }
}
