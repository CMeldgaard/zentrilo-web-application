<?php
namespace Zentrilo\Modules\Sessions;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->list ="";
          $this->contact ="";
  }

  public function set($type,$data){
      $this->$type($data);
  }

  private function user($data){
    if($data[0]["userUUID"]){
      $data = $data[0];
    }
    $_SESSION['userUUID'] = $data["userUUID"];
    $_SESSION['userFname'] = $data["fName"];
    $_SESSION['userLname'] = $data["lName"];
    $_SESSION['gravatar'] = $data["gravatar"];
  }

  private function account($UUID){
    $_SESSION['accountUUID'] = $UUID;
    $Accounts = Autoloader::newLoad('Modules\\Accounts');
    $name = $Accounts->accountName($UUID);
    $_SESSION['accountName'] = $name;
  }

  public function destroy(){
    unset($_SESSION['accountUUID']);
    unset($_SESSION['sessionUUID']);
    setcookie("ZentriloCSRF", "", time()-3600);
    header("Location: /signin");
    die();
  }

}
