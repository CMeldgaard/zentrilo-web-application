<?php
namespace Zentrilo\Modules\History;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->list ="";
  }
 public function getHistory($refrenceUUID){
   $this->list = $this->Model->get($refrenceUUID);
   return $this;
 }

 public function buildList(){
   $history = $this->list;
   $datetime = Autoloader::newLoad('Modules\\Timing');
   if(count($history)>0){
     foreach($history as &$item) {
        $item['created'] = $datetime->humanTime($item['created']);
    }
     $this->get_view()->set('histories',$history);
     return $this->get_view()->moduleRender('history/view/history-panel');
   }else{
     $this->get_view()->set('icon','speech');
     $this->get_view()->set('text','Ingen historik registreret.');
     return $this->get_view()->moduleRender('history/view/empty-panel');
   }

 }

 public function lastUpdate(){
   $history = $this->list;
   if(count($history)>0){
     $datetime = Autoloader::newLoad('Modules\\Timing');
     return $datetime->humanTime($history[0]['created'],"year");
   }else{
     return "Ingen historik";
   }
 }

}
