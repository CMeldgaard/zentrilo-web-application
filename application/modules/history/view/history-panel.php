<div class="historyPanel">
  <?php foreach($histories as $history):?>
  <div class="history-entry">
    <div class="entry-detail">
      <div class="icon">
        <i class="icon-speech"></i>
      </div>
      <div class="creation">
        <?php echo $history['created'];?> siden
      </div>
      <div class="entry-content">
        <p class="creator"><?php echo $history['typeText'];?> af <?php echo $history['fName'].' '.$history['lName'];?></p>
        <p><?php echo $history['content'];?></p>
      </div>
    </div>
  </div>
<?php endforeach?>
</div>
