<?php
namespace Zentrilo\Modules\History;
use Autoloader;
class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function get($refrenceUUID){
      return $this->coredb
                  ->join('users','users.userUUID=contacts_persons_history.creatorUUID')
                  ->join('types_history','contacts_persons_history.type=types_history.typeUUID')
                  ->where("referenceUUID",$refrenceUUID)
                  ->orderby("created","desc")
                  ->get("contacts_persons_history");
    }
}
