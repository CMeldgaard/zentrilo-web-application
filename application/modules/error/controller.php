<?php
namespace Zentrilo\Modules\Error;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->data ="";
  }

  public function verifyURL($controller,$method){

  }

  private function verifyController($controller){

    return $this;
  }

  private function verifyMethod($method){
    if (!class_exists($controller) and !empty($controller)){
        $controller = 'errors';
        $action = 'show404';
    }
    if (!(int)method_exists($controller, $action) and !empty($action)) {
        $controller = 'errors';
        $action = 'show500';
    }
  }

}
