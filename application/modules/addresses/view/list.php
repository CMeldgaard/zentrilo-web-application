<?php foreach ($addresses as $address):?>
<div class="adress">
<?php echo $address['street'];?>, <?php echo $address['city'];?>, <?php echo $address['postal'];?>

  <div class="tools">
    <a href="http://www.google.com/maps?q=<?php echo urlencode($address['street'].', '.$address['city'].', '.$address['postal']);?>" target="_blank">
      <i class="icon-location-pin"></i>
    </a>
    <a href="http://www.google.com/maps?daddr=<?php echo urlencode($address['street'].', '.$address['city'].', '.$address['postal']);?>" target="_blank">
      <i class="icon-map"></i>
    </a>
    <a href="">
      <i class="icon-paper-clip"></i>
    </a>
    <a href="">
      <i class="icon-list"></i>
    </a>
  </div>
</div>
<?php endforeach;?>
