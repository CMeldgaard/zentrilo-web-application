<?php
namespace Zentrilo\Modules\Addresses;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->list ="";
  }
 public function load($UUID){
   $this->list = $this->Model->load($UUID);
   return $this;
 }

 public function buildHTLM($data){
   if($data[0]["street"]){
     $this->get_view()->set('addresses',$data);
     return $this->get_view()->moduleRender('addresses/view/list');
   }else{
     return "";
   }

 }

 public function getData(){
   $list = $this->list;
   return $list;
 }
}
