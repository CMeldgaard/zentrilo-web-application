<?php
namespace Zentrilo\Modules\Addresses;

class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function load($referenceUUID){
      return $this->coredb
                  ->where("referenceUUID",$referenceUUID)
                  ->get("addresses");
    }

    public function getData($UUID){
      return $this->coredb
                  ->join('contacts_companies','contacts_persons.companyUUID=contacts_companies.companyUUID')
                  ->where("contacts_persons.personUUID",$UUID)
                  ->get("contacts_persons");
    }

    public function getUUID($id){
      $data = $this->coredb
                  ->where("entityID",$id)
                  ->get("contacts_persons");
      return $data[0]["personUUID"];
    }

    public function insert($data){
      $accountUUID = $_SESSION['accountUUID'];
      $contactData = Array ("accountUUID" => $accountUUID,
               "companyUUID" => $data["companyUUID"],
               "fName" => $data["fName"],
               "lName" => $data["lName"],
               "worktitle" => $data["worktitle"],
               "phone" => $data["phone"],
               "email" => $data["email"]
             );
      return $this->coredb->insert ('contacts_persons', $contactData);
    }
}
