<?php
namespace Zentrilo\Modules\Users;
use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
  }

  public function uniqUsername($username){
    return $this->Model->checkUniq($username);
  }

  public function createUser($data){
    //Generate hashed password
    $crypt = Autoloader::newLoad('Modules\\Crypt');
    $password = $data["password"];
    $hashed = $crypt->hash($password);

    //Get gravatar for user
    $gravatar = Autoloader::newLoad('Modules\\Gravatar');
    $grav_url = $gravatar->getGravatar($data["email"],$data["name"]);

    //Create user and return entityID
    $username =$data["username"];
    $name =$data["name"];
    $email =$data["email"];
    return $this->Model->create($username,$hashed,$name,$email,$grav_url);
  }

  public function getUUID($entityID){
    $user = $this->Model->getUser($entityID,"entityID");
    $UUID = $user[0]["userUUID"];
    return $UUID;
  }

  public function account(){
    
  }
}
