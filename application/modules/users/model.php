<?php
namespace Zentrilo\Modules\Users;
use Autoloader;
class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function checkUniq($username){
      $user = $this->coredb
                 ->where('username',$username)
                 ->get('users');
      return $user;
    }

    public function create($username,$hashed,$name,$email,$grav_url){
      $nameExp = explode(" ", $name);
      $fName = $nameExp[0];
      $lName = "";
      if(count($nameExp)>1){
        $lName = $nameExp[1];
      }
      $data = Array ("username" => $username,
               "password" => $hashed,
               "fname" => $fName,
               "lname" => $lName,
               "email" => $email,
               "gravatar" =>$grav_url
              );
      $id = $this->coredb->insert ('users', $data);
      return $id;
    }

    public function getUser($ID,$column="userUUID"){
      $user = $this->coredb
                 ->where($column,$ID)
                 ->get('users');
      return $user;
    }
}
