<?php
namespace Zentrilo\Modules\Crypt;

class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
  }

  public function verifyHashedPassword($password,$storedPassword){
    $check = password_verify($password,$storedPassword);
    return $check;
  }

  public function hash($data){
    $salt = $this->generateSalt();
    return password_hash($data,PASSWORD_BCRYPT);
  }

  public function generateSalt(){
    return uniqid(mt_rand(), true);
  }
}
