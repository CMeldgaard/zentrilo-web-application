<?php
namespace Zentrilo\Modules\Timing;
use Autoloader;
class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function get(){
      $accountUUID = $_SESSION['accountUUID'];
      return $this->coredb
                  ->join('contacts_companies','contacts_persons.companyUUID=contacts_companies.companyUUID')
                  ->where("accountUUID",$accountUUID)
                  ->orderby("fName","asc")
                  ->get("contacts_persons");
    }
}
