<?php
namespace Zentrilo\Modules\Timing;
Use DateTime;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->list ="";
  }
 public function humanTime($time,$break='day'){
   // TODO: Ret til så den regner rigtig og kan vælge hvor den skal breake
    $time = strtotime($time);
    $time = time() - $time; // to get the time since that moment
    if($time>31536000){
      #Years
      $numberOfUnits = $this->convert($time,31536000,$break);
      return $numberOfUnits;
    }elseif($time>2592000){
      #Months
      $numberOfUnits = $this->convert($time,2592000,$break);
      return $numberOfUnits;
    }elseif($time>604800){
      #Weeks
      $numberOfUnits = $this->convert($time,604800,$break);
      return $numberOfUnits;
    }elseif($time>86400){
      #Days
      $numberOfUnits = floor($time / 86400);
      return $numberOfUnits.' dag'.(($numberOfUnits>1)?'e':'');
    }elseif($time>3600){
      #Hours
      $numberOfUnits = floor($time / 3600);
      return $numberOfUnits.' time'.(($numberOfUnits>1)?'r':'');
    }elseif($time>60){
      #Minutes
      $numberOfUnits = floor($time / 3600);
      return $numberOfUnits.' minut'.(($numberOfUnits>1)?'er':'');
    }else{
      #Seconds
      $numberOfUnits = $this->convert($time,1,$break);
      return $numberOfUnits;
    }
 }

 protected function convert($time,$units,$option){
   $numberOfUnits = floor($time / $units);
   return $numberOfUnits.' time'.(($numberOfUnits>1)?'r':'');
 }

}
