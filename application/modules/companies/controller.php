<?php
namespace Zentrilo\Modules\Companies;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->data ="";
          $this->accountUUID = $_SESSION['accountUUID'];
  }

  public function load($UUID="",$column="companyUUID"){
    $this->data = $this->Model->get($UUID,$column);
    return $this;
  }

  public function getData(){
    $company = $this->data;
    return $company;
  }

 public function getHeader(){
   $company = $this->data;
   $this->get_view()->set('company',$company[0]);
   //Get social links
   $social = Autoloader::newLoad('Modules\\Social');
   $links = $social->getLinks($company[0]['companyUUID'])->build();
   $this->get_view()->set('links',$links);

   return $this->get_view()->moduleRender('companies/view/header-company');
 }

 public function fullList(){
   $companies = $this->data;
   $this->get_view()->set('companies',$companies);
   return $this->get_view()->moduleRender('companies/view/company-line');
 }

 public function quickCreate($companyName){
   //Sanitize data to prevent XSS
   $companyName = $this->sanitize($companyName);
   //Create insert array
   $data = Array ("accountUUID" => $this->accountUUID,
            "companyName" => $companyName,
          );
  return $this->Model->insert($data);
 }

 public function create($data){
   //Sanitize data to prevent XSS
   $data = $this->sanitize($data);
   //Generate SQL array
   $companyData = $this->buildArray($data);
   //Create user based on sent data
   $contactID = $this->Model->insert($companyData);
 }

 public function delete($uuid){
   $this->Model->delete($uuid);
 }

//Update selected company
 public function update($data,$UUID){
   //Sanitize data to prevent XSS
   $data = $this->sanitize($data);
   //Generate SQL array
   $companyData = $this->buildArray($data);
   //Update company
   $contactID = $this->Model->update($companyData,$UUID);
 }

//Generate array for data insertion
 protected function buildArray($data){
   $accountUUID = $_SESSION['accountUUID'];
   $companyData = Array ("accountUUID" => $accountUUID,
            "companyName" => $data["companyName"],
            "companyPhone" => $data["phone"],
            "companyEmail" => $data["email"],
            "companyAbout" => $data["about"],
            "street" => $data["street"],
            "city" => $data["city"],
            "postal" => $data["postal"]
          );
    return $companyData;
 }

}
