<?php
namespace Zentrilo\Modules\Companies;

class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
        $this->accountUUID = $_SESSION['accountUUID'];
    }

    public function get($UUID,$column){
      if($UUID){
        //Want specific person, so add to query
        return $this->getCompany($UUID,$column);
      }else{
        //Get all connected persons
        return $this->getConnected();
      }
    }

    private function getCompany($UUID,$column){
      return $this->coredb
                ->where('accountUUID',$this->accountUUID)
                ->where($column,$UUID)
                ->get("contacts_companies");
    }

    private function getConnected(){
      return $this->coredb
                  ->where('accountUUID',$this->accountUUID)
                  ->get("contacts_companies");
    }

    public function insert($data){
      return $this->coredb->insert('contacts_companies', $data);
    }

    public function update($data,$UUID){
      $accountUUID = $_SESSION['accountUUID'];
      return $this->coredb
                  ->where('companyUUID',$UUID)
                  ->where('accountUUID',$accountUUID)
                  ->update ('contacts_companies', $data);
    }

    public function delete($UUID){
      $accountUUID = $_SESSION['accountUUID'];
      $this->coredb
                  ->where('companyUUID',$UUID)
                  ->where('accountUUID',$accountUUID)
                  ->delete ('contacts_companies');
    }
}
