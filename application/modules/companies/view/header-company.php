<div class="header-text">
  <h1><?php echo $company['companyName']; ?>
    <div data-uk-dropdown="{mode:'click'}">
      <div><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
      <div class="uk-dropdown">
          <ul class="uk-nav uk-nav-dropdown contact-tools">
              <li><a href="#" class="editContact" data-uuid="<?php echo $company['companyUUID']; ?>" data-type="company">Rediger virksomhed</a></li>
              <li><a href="#">Tilføj projekt</a></li>
              <li><a href="#">Tilføj opgave</a></li>
              <li><a href="#">Esporter som vCard</a></li>
              <li><a href="#" class="delete deleteContact" data-uuid="<?php echo $company['companyUUID']; ?>" data-type="company">Slet virksomhed</a></li>
          </ul>
      </div>
    </div>
  </h1>
  <div class="subtitle">
    <?php if($company['primaryContact']): ?>
    <span>Primær kontakt</span>
  <?php else:?>
    <span>Ingen primær kontakt valgt</span>
    <?php endif;?>
  </div>
  <?php echo $links;?>
</div>
