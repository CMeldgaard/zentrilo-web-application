<div class="add-modal scroll">
  <div class="nano-content">
    <div class="modal-header">
        Opret ny virksomhed
    </div>
    <form class="app-form uk-form small" method="post" action="">
      <fieldset>
        <div class="uk-grid uk-margin uk-grid-right">
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="companyName" class="">Firmanavn</label>
              <input type="text" name="companyName" id="companyName">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="phone" class="">Telefon nr.</label>
              <input type="text" name="phone" id="phone">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="email" class="">Email adresse</label>
              <input type="text" name="email" id="email">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-2-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="street" class="">Adresse</label>
              <input type="text" name="street" id="street">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="postal" class="">Post nr.</label>
              <input type="text" name="postal" id="postal">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="city" class="">By</label>
              <input type="text" name="city" id="city">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-2-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="about" class="">Omkring</label>
              <textarea name="about" id="about" rows="3" ></textarea>
            </div>
          </div>
        </div>
      </div>
      </fieldset>
      <div class="formtools">
          <button class="cancel modal-toggle" data-uk-tooltip="{pos:'top-left',animation:'true'}" title="Anuller">
              Anuller <i class="fa fa-close"></i>
          </button>
          <button class="confirm modal-save" href="/contacts/savecompany" data-uk-tooltip="{pos:'top-left',animation:'true'}" title="Gem virksomhed">
              Gem <i class="fa fa-floppy-o"></i>
          </button>
      </div>
    </form>
  </div>
</div>
