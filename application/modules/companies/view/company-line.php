<ul class="contact-company">
<?php
foreach ($companies as $company):
?>
<li class="contact-line getCompany" data-uuid="<?php echo $company["companyUUID"];?>" data-filter="<?php echo $company["companyName"];?>">
  <div class="clearfix uk-grid">
    <div class="avatar uk-width-1-10">
        <img class="img-circle img-responsive pull-left" src="/assets/images/org_avatar.png" alt="">
    </div>
    <div class="uk-width-9-10">
      <span class="name">
          <?php echo $company["companyName"];?>
      </span>
      <div class="details">
        <?php if($company["companyEmail"]):?>
          <div class="detail">
            E: <span><i class="icon-envelope"></i><a href="mailto:<?php echo $company["companyEmail"];?>"><?php echo $company["companyEmail"];?></a></span>
          </div>
        <?php endif;?>
        <?php if($company["companyPhone"]):?>
          <div class="detail">
            T: <span><i class="icon-phone"></i><a href="tel:<?php echo $company["companyPhone"];?>"><?php echo $company["companyPhone"];?></a></span>
          </div>
        <?php endif;?>
      </div>
    </div>
  </div>
</li>
<?php
endforeach
?>
</ul>
