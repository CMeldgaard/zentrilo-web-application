<div class="nano-content">
    <div class="modal-header">
        Rediger virksomhed
    </div>
    <form class="uk-form small app-form edit-form" data-uuid="<?php echo $company["companyUUID"];?>" data-type="company">
      <fieldset>
        <div class="uk-grid uk-margin uk-grid-right">
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="companyName" class="">Virksomhedsnavn</label>
              <input type="text" name="companyName" id="companyName" value="<?php echo $company["companyName"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="companyPhone" class="">Telefon nr.</label>
              <input type="text" name="companyPhone" id="companyPhone" value="<?php echo $company["companyPhone"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="companyEmail" class="">Email adresse</label>
              <input type="text" name="companyEmail" id="companyEmail" value="<?php echo $company["companyEmail"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-2-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="street" class="">Adresse</label>
              <input type="text" name="street" id="street" value="<?php echo $company["street"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="postal" class="">Post nr.</label>
              <input type="text" name="postal" id="postal" value="<?php echo $company["postal"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="city" class="">By</label>
              <input type="text" name="city" id="city" value="<?php echo $company["city"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-2-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="companyAbout" class="">Omkring</label>
              <textarea name="companyAbout" id="companyAbout" rows="3" ><?php echo $company["companyAbout"];?></textarea>
            </div>
          </div>
        </div>
      </div>
      </fieldset>
      <div class="formtools">
          <button class="cancel cancel-task" data-uk-tooltip="{pos:'top-left',animation:'true'}" title="Anuller" data-uuid="<?php echo $company["companyUUID"];?>" data-type="company">
              Annuller <i class="fa fa-close"></i>
          </button>
          <button class="confirm contact-update" data-uk-tooltip="{pos:'top-left',animation:'true'}" title="Opdater kontakt">
              Gem <i class="fa fa-floppy-o"></i>
          </button>
      </div>
    </form>
</div>
