<?php
namespace Zentrilo\Modules\Accounts;
use Autoloader;
class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function getAccounts($UUID){
      return $this->coredb
                  ->join('accounts','accounts.accountUUID=users_accounts.accountUUID')
                  ->where('users_accounts.userUUID',$UUID)
                  ->orderBy('accounts.accountName','asc')
                  ->get('users_accounts');
    }

    public function getColumn($UUID,$column){
      $account = $this->coredb
                 ->where('accountUUID',$UUID)
                 ->get('accounts');
      $name = $account[0][$column];
      return $name;
    }

    public function create($companyName){
      $data = Array ("accountName" => $companyName);
      $id = $this->coredb->insert ('accounts', $data);
      return $id;
    }

    public function getAccount($ID,$column="accountUUID"){
      $account = $this->coredb
                 ->where($column,$ID)
                 ->get('accounts');
      return $account;
    }

    public function link($userUUID, $accountUUID){
      $data = Array ("userUUID" => $userUUID,
                     "accountUUID" => $accountUUID);
      $id = $this->coredb->insert ('users_accounts', $data);
    }
}
