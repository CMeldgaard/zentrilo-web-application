<?php
namespace Zentrilo\Modules\Accounts;

class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
  }

  public function connectedAccounts($UUID){
    return $this->Model->getAccounts($UUID);
  }

  public function accountName($UUID){
    return $this->Model->getColumn($UUID,'accountName');
    $account[0][$column];
  }

  public function createAccount($companyname){
    return $this->Model->create($companyname);
  }

  public function getUUID($entityID){
    $account = $this->Model->getAccount($entityID,"entityID");
    $UUID = $account[0]["accountUUID"];
    return $UUID;
  }

  public function linkAccount($userUUID,$accountUUID){
    $account = $this->Model->link($userUUID,$accountUUID);
  }
}
