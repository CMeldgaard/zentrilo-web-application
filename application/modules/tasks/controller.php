<?php
namespace Zentrilo\Modules\Tasks;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->data ="";
          $this->accountUUID = $_SESSION['accountUUID'];
  }

//Load data
 public function load($UUID="",$column="referenceUUID"){
   $this->data = $this->Model->get($UUID,$column);
   return $this;
 }

//Return the data object as a array
 public function getData(){
    //Get data as Array
   $tasks = $this->data;
   //Return the array
   return $tasks;
 }

 public function taskCount(){
   //Get data as Array
   $tasks = $this->data;
   //Count tasks
   $count = count($tasks);
   return $count;
 }

 public function getList(){
   //Get data as Array
  $tasks = $this->data;

  if(count($tasks)>0){
    //Bind data to view
    $this->get_view()->set('tasks',$tasks);
    //Return HTML
    return $this->get_view()->moduleRender('tasks/view/list');
  }else{
    $this->get_view()->set('icon','list');
    $this->get_view()->set('text','Ingen opgaver registreret.');
    return $this->get_view()->moduleRender('tasks/view/empty-panel');
  }
 }

}
