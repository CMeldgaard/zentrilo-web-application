<?php
namespace Zentrilo\Modules\Tasks;

class Model extends \Zentrilo\Core\Model {

  protected $accountUUID;

  public function __construct() {
      // Load core model functions
      parent::__construct();
      $this->accountUUID = $_SESSION['accountUUID'];
  }
  //SELECT query
  public function get($UUID,$column){
    //Check if specific person thats needed
    if($UUID){
      //Want specific person
      return $this->getSpecific($UUID,$column);
    }else{
      //Get all connected persons
      return $this->getAll();
    }
  }

  private function getSpecific($UUID,$column){
    //Build query
    return $this->coredb
      ->where("accountUUID",$this->accountUUID)
      ->where($column,$UUID)
      ->orderby("taskDeadline","asc")
      ->get("tasks");
  }

  private function getAll(){
    //Build query
    return $this->coredb
      ->where("accountUUID",$this->accountUUID)
      ->orderby("taskDeadline","asc")
      ->get("tasks");
  }
}
