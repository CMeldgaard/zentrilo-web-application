<ul class="task-list form">
  <?php foreach ($tasks as $task):?>
  <li class="item line <?php if($task["completed"]==1):?>completed<?php endif;?>" id="<?php echo $task["taskUUID"];?>">
      <div class="checkbox">
        <input id="<?php echo $task["taskUUID"];?>" <?php if($task["completed"]==1):?>checked="checked"<?php endif;?> type="checkbox" class="small">
      </div>
      <div class="detail">
        <span class="task-title">
          <a href="javascript: void(0);">
            <?php echo $task["taskName"];?>
          </a>
          <span class="more-detail"><?php echo $task["taskDetails"];?></span>
          <span class="label overdue">Forfalden</span>
          <span class="label highlight">Møde</span>
        </span>
        <span class="task-deadline">
          <i class="icon-clock"></i><?php echo $task["taskDeadline"];?>
        </span>
      </div>
  </li>
<?php endforeach;?>
</ul>
