<?php
namespace Zentrilo\Modules\Contactgroups;

class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function get($search,$column){
      $accountUUID = $_SESSION['accountUUID'];
      return $this->coredb
                  ->where('accountUUID',$accountUUID)
                  ->where($column,$search)
                  ->orderby("groupName","asc")
                  ->get("contacts_groups");
    }

    public function getLinked($UUID,$type){
      $accountUUID = $_SESSION['accountUUID'];
      if($type=="persons"){
        $column = "personUUID";
      }else{
        $column = "companyUUID";
      }
      return $this->coredb
                  ->join("contacts_groups_".$type,'contacts_groups_'.$type.'.contactGroupUUID=contacts_groups.contactGroupUUID')
                  ->join("contacts_".$type,'contacts_'.$type.'.'.$column.'=contacts_groups_'.$type.'.'.$column)
                  ->where("contacts_groups.contactGroupUUID",$UUID)
                  ->where("contacts_groups.accountUUID",$accountUUID)
                  ->get("contacts_groups");
    }

    protected function getUUID($id){
      $data = $this->coredb
                  ->where("entityID",$id)
                  ->get("contacts_groups");
      return $data[0]["contactGroupUUID"];
    }

    public function insert($data){
      $entityID = $this->coredb->insert ('contacts_groups', $data);
      $UUID = $this->getUUID($entityID);
      return $UUID;
    }

    public function linkPersons($data){
      $keys = Array("personUUID", "contactGroupUUID");
      return $this->coredb
                  ->insertMulti("contacts_groups_persons",$data,$keys);
    }

    public function linkCompanies($data){
      $keys = Array("companyUUID", "contactGroupUUID");
      return $this->coredb
                  ->insertMulti("contacts_groups_companies",$data,$keys);
    }
}
