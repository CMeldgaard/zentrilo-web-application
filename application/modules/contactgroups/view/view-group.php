<div class="nano-content">
  <div class="block">
    <div class="header">
      <div class="header-image">
          <div class="changeable-avatar">
              <img class="avatar" src="/assets/images/person_avatar.png" alt="picture">
              <span class="changeable-avatar-link" onclick="">
                Skift
              </span>
          </div>
      </div>
      <?php echo $headerText;?>
      <?php if($referer):?>
      <i class="fa fa-arrow-left referer-toggle" aria-hidden="true" data-type="<?php echo $referer;?>" data-uuid="<?php echo $refererUUID;?>"  data-uk-tooltip="{pos:'bottom-left',animation:'true'}" title="Tilbage"></i>
      <?php endif; ?>
      <i class="fa fa-times content-slider-toggle" aria-hidden="true"></i>
    </div>
    <div class="uk-grid uk-margin">
      <div class="uk-width-small-2-2">
        <span class="heading">Omkring</span>
        <?php echo $group["groupAbout"];?>
      </div>
    </div>
      <div class="uk-grid uk-margin">
        <div class="uk-width-small-1-2 uk-width-large-1-2">
          <span class="heading">Kontaktpersoner</span>
          <?php foreach ($linkedPersons as $person):?>
              <span class="link getCompany" onclick="getContact('<?php echo $person['personUUID']; ?>','group','<?php echo $person['contactGroupUUID']; ?>')"><?php echo $person["fName"];?> <?php echo $person["lName"];?></span><br />
          <?php endforeach?>
        </div>
        <div class="uk-width-small-1-2 uk-width-large-1-2">
          <span class="heading">Virksomheder</span>
          <?php foreach ($linkedCompanies as $company):?>
            <span class="link getCompany" onclick="getCompany('<?php echo $company['companyUUID']; ?>','group','<?php echo $group['contactGroupUUID']; ?>')"><?php echo $company['companyName']; ?></span><br>
          <?php endforeach?>
        </div>
      </div>
  </div>

  <div class="block full">
    <ul class="summary">
      <li>
          <div class="icon"><span class="icon-folder"></span></div> 0
          <h4>Åbne projekter</h4>
      </li>
      <li>
          <div class="icon"><span class="icon-calendar"></span></div>
              <?php echo $lastUpdate;?>
          <h4>Seneste opdatering</h4>
      </li>
      <li>
          <div class="icon"><span class="icon-graph"></span></div>
          kr 0
          <h4>Pipeline Værdi</h4>
      </li>
  </ul>
  <ul class="uk-tab contact-tab" data-uk-tab="{connect:'#person-content'}">
      <li><a href="">Historik</a></li>
      <li><a href="">Filer <span class="rounded-counter">0</span></a></li>
      <li><a href="">Pipeline <span class="rounded-counter">0</span></a></li>
      <li><a href="">Projekter <span class="rounded-counter">0</span></a></li>
      <li><a href="">Opgaver <span class="rounded-counter"><?php echo $taskCount;?></span></a></li>
  </ul>
  <ul id="person-content" class="uk-switcher">
      <li class="panel">
        <div class="panel-tools">
          <a href="#">Ny note</a>
          <a href="#">Nyt opkald</a>
          <a href="#">Ny email</a>
        </div>
        <?php echo $history;?>
      </li>
      <li class="panel">
        <div class="panel-tools">
          <a href="#">Upload fil</a>
        </div>
        <div class="empty-state">
            <span class="icon-picture"></span>
            <p>Ingen filer registreret for denne kontakt.</p>
        </div>
      </li>
      <li class="panel">
        <div class="panel-tools">
          <a href="#">Opret mulighed</a>
        </div>
        <div class="empty-state">
            <span class="icon-rocket"></span>
            <p>Intet i pipelinen for denne kontakt.</p>
        </div>
      </li>
      <li class="panel">
        <div class="panel-tools">
          <a href="#">Opret projekt</a>
        </div>
        <div class="empty-state">
            <span class="icon-briefcase"></span>
            <p>Ingen projekter er tilknyttet denne kontakt.</p>
        </div>
      </li>
      <li class="panel">
        <div class="panel-tools">
          <a href="#">Opret opgave</a>
        </div>
        <?php echo $tasks;?>
      </li>
  </ul>
  </div>
</div>
