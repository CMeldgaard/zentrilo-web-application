<div class="header-text">
  <h1><?php echo $group['groupName']; ?>

    <div data-uk-dropdown="{mode:'click'}">
      <div><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
      <div class="uk-dropdown">
          <ul class="uk-nav uk-nav-dropdown contact-tools">
              <li><a href="#" class="editContact" data-uuid="<?php echo $group['contactGroupUUID']; ?>" data-type="group">Rediger gruppe</a></li>
              <li><a href="#">Tilføj projekt</a></li>
              <li><a href="#">Tilføj opgave</a></li>
              <li><a href="#" class="delete deleteContact" data-uuid="<?php echo $group['contactGroupUUID']; ?>" data-type="group">Slet gruppe</a></li>
          </ul>
      </div>
    </div>
  </h1>
</div>
