<ul class="contact-group">
<?php
foreach ($groups as $group):
?>
<li class="contact-line getGroup" data-uuid="<?php echo $group["contactGroupUUID"];?>" data-filter="<?php echo $group["groupName"];?>">
  <div class="clearfix uk-grid">
    <div class="avatar uk-width-1-10">
        <img class="img-circle img-responsive pull-left" src="/assets/images/person_avatar.png" alt="">
    </div>
    <div class="uk-width-9-10">
      <span class="name">
          <?php echo $group["groupName"];?>
      </span>
    </div>
  </div>
</li>
<?php
endforeach
?>
</ul>
