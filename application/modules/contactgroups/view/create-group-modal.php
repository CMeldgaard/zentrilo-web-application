<div class="add-modal scroll">
  <div class="nano-content">
    <div class="modal-header">
        Opret ny gruppe
    </div>
    <form class="app-form uk-form small" method="post" action="">
      <fieldset>
        <div class="uk-grid uk-margin uk-grid-right">
        <div class="uk-width-medium-2-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="groupName" class="">Gruppenavn</label>
              <input type="text" name="groupName" id="groupName">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-2-2">
          <div class="line">
            <?php
            ?>
            <div class="field-wrapper">
              <label for="membersPerson" class="">Kontaktpersoner</label>
              <select name="membersPerson[]" id='persons' multiple='multiple'>
                  <?php
                  echo $contactOptions;
                  ?>
              </select>
            </div>
          </div>
        </div>
        <div class="uk-width-medium-2-2">
          <div class="line">
            <?php
            ?>
            <div class="field-wrapper">
              <label for="membersCompany" class="">Virksomheder</label>
              <select name="membersCompany[]" id='companies' multiple='multiple'>
                  <?php
                  echo $companyOptions;
                  ?>
              </select>
            </div>
          </div>
        </div>
        <div class="uk-width-medium-2-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="groupAbout" class="">Omkring</label>
              <textarea name="groupAbout" id="groupAbout" rows="3" ></textarea>
            </div>
          </div>
        </div>
      </div>
      </fieldset>
      <div class="formtools">
          <button class="cancel modal-toggle" data-uk-tooltip="{pos:'top-left',animation:'true'}" title="Anuller">
              Annuller <i class="fa fa-close"></i>
          </button>
          <button class="confirm modal-save" href="/contacts/savegroup" data-uk-tooltip="{pos:'top-left',animation:'true'}" title="Gem kontakt">
              Gem <i class="fa fa-floppy-o"></i>
          </button>
      </div>
    </form>
  </div>
</div>
<script>
$('#persons').multiSelect({
  selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Søg'>",
  selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Søg'>",
  afterInit: function(ms){
    var that = this,
        $selectableSearch = that.$selectableUl.prev(),
        $selectionSearch = that.$selectionUl.prev(),
        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
    .on('keydown', function(e){
      if (e.which === 40){
        that.$selectableUl.focus();
        return false;
      }
    });

    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
    .on('keydown', function(e){
      if (e.which == 40){
        that.$selectionUl.focus();
        return false;
      }
    });
  },
  afterSelect: function(){
    this.qs1.cache();
    this.qs2.cache();
  },
  afterDeselect: function(){
    this.qs1.cache();
    this.qs2.cache();
  }
});
$('#companies').multiSelect({
  selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Søg'>",
  selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Søg'>",
  afterInit: function(ms){
    var that = this,
        $selectableSearch = that.$selectableUl.prev(),
        $selectionSearch = that.$selectionUl.prev(),
        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
    .on('keydown', function(e){
      if (e.which === 40){
        that.$selectableUl.focus();
        return false;
      }
    });

    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
    .on('keydown', function(e){
      if (e.which == 40){
        that.$selectionUl.focus();
        return false;
      }
    });
  },
  afterSelect: function(){
    this.qs1.cache();
    this.qs2.cache();
  },
  afterDeselect: function(){
    this.qs1.cache();
    this.qs2.cache();
  }
});
</script>
