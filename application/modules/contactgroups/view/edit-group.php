<div class="nano-content">
    <div class="modal-header">
        Rediger kontaktperson
    </div>
    <form class="uk-form small app-form edit-form" data-uuid="<?php echo $contact["personUUID"];?>" data-type="person">
      <fieldset>
        <div class="uk-grid uk-margin uk-grid-right">
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="fName" class="">Fornavn</label>
              <input type="text" name="fName" id="fName" value="<?php echo $contact["fName"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="lName" class="">Efternavn</label>
              <input type="text" name="lName" id="lName" value="<?php echo $contact["lName"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="worktitle" class="">Job titel</label>
              <input type="text" name="worktitle" id="worktitle" value="<?php echo $contact["worktitle"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="companyName" class="">Virksomhed</label>
              <input class="companyName" type="text" name="companyName" id="companyName" value="<?php echo $contact["companyName"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="phone" class="">Telefon nr.</label>
              <input type="text" name="phone" id="phone" value="<?php echo $contact["phone"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="email" class="">Email adresse</label>
              <input type="text" name="email" id="email" value="<?php echo $contact["email"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-2-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="street" class="">Adresse</label>
              <input type="text" name="street" id="street" value="<?php echo $contact["street"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="postal" class="">Post nr.</label>
              <input type="text" name="postal" id="postal" value="<?php echo $contact["postal"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-1-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="city" class="">By</label>
              <input type="text" name="city" id="city" value="<?php echo $contact["city"];?>">
            </div>
          </div>
        </div>
        <div class="uk-width-medium-2-2">
          <div class="line">
            <div class="field-wrapper">
              <label for="about" class="">Omkring</label>
              <textarea name="about" id="about" rows="3" ><?php echo $contact["about"];?></textarea>
            </div>
          </div>
        </div>
      </div>
      </fieldset>
      <div class="floating-action">
          <button class="btn red cancel-task" data-uk-tooltip="{pos:'top-left',animation:'true'}" title="Anuller" data-uuid="<?php echo $contact["personUUID"];?>" data-type="person">
              <i class="fa fa-close"></i>
          </button>
          <button class="btn contact-update" data-uk-tooltip="{pos:'top-left',animation:'true'}" title="Opdater kontakt">
              <i class="fa fa-floppy-o"></i>
          </button>
      </div>
    </form>
</div>
<script>
var companies = <?php echo $companies?>;
  $('.companyName').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  },
  {
    name: 'company',
    source: substringMatcher(companies)
  });
</script>
