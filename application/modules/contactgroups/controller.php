<?php
namespace Zentrilo\Modules\Contactgroups;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->list ="";
          $this->group ="";
  }

  public function load($search="",$column="accountUUID"){
    if($search==""){
      $search = $_SESSION['accountUUID'];
    }
    $this->list = $this->Model->get($search,$column);
    return $this;
  }

  public function getLinkedPersons($UUID){
    $this->list = $this->Model->getLinked($UUID,"persons");
    return $this;
  }

  public function getLinkedCompanies($UUID){
    $this->list = $this->Model->getLinked($UUID,"companies");
    return $this;
  }

  public function getData(){
    $list = $this->list;
    return $list;
  }

  public function fullList(){
    $groups = $this->list;
    $this->get_view()->set('groups',$groups);
    return $this->get_view()->moduleRender('contactgroups/view/group-line');
  }

  public function getHeader(){
    $group = $this->list;
    $this->get_view()->set('group',$group[0]);
    return $this->get_view()->moduleRender('contactgroups/view/header-group');
  }

 public function create($data){
   //Sanitize data to prevent XSS
   //$data = $this->sanitize($data);
   //Generate SQL array
   $companyData = $this->buildArray($data);
   //Create group
   $groupID = $this->Model->insert($companyData);
   //Link selected persons
   $persons = $data["membersPerson"];
   $personsArray = array();
   foreach ($persons as $person) {
     $personsArray[]=array($person,$groupID);
   }
   $this->Model->linkPersons($personsArray);
   //Link select companies
   $companies = $data["membersCompany"];
   $companiesArray = array();
   foreach ($companies as $company) {
     $companiesArray[]=array($company,$groupID);
   }
   $this->Model->linkCompanies($companiesArray);
 }

 public function update($data){

   //To update, first get all current connected contacts. Compare to new list, and remove/add based on that
   // use array_diff() to find contacts that needs to be deleted and inserted

 }

 protected function buildArray($data){
   $accountUUID = $_SESSION['accountUUID'];
   $companyData = Array ("accountUUID" => $accountUUID,
            "groupName" => $data["groupName"],
            "groupAbout" => $data["groupAbout"]
          );
    return $companyData;
 }

}
