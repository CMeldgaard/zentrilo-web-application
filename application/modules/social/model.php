<?php
namespace Zentrilo\Modules\Social;
use Autoloader;
class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function get($refrenceUUID){
      return $this->coredb
                  ->join('social_links_type','social_links.socialTypeUUID=social_links_type.typeUUID')
                  ->where("referenceUUID",$refrenceUUID)
                  ->get("social_links");
    }
}
