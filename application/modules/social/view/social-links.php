<div class="social-links">
  <?php foreach($links as $link):?>
    <a class="social-icon <?php echo strtolower($link["name"]);?>" href="<?php echo $link['URL'];?>" title="<?php echo $link['name'];?>" rel="noreferrer" target="_blank">
      <i class="<?php echo $link['icon'];?>" aria-hidden="true"></i>
    </a>
  <?php
  endforeach
  ?>
</div>
