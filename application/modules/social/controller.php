<?php
namespace Zentrilo\Modules\Social;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
          $this->list ="";
  }
 public function getLinks($refrenceUUID){
   $this->list = $this->Model->get($refrenceUUID);
   return $this;
 }

 public function build(){

   $links = $this->list;
   $this->get_view()->set('links',$links);
   //Get social links
   return $this->get_view()->moduleRender('social/view/social-links');
 }

}
