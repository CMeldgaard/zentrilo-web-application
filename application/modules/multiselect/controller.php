<?php
namespace Zentrilo\Modules\Multiselect;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
  }


 public function getOptions($data,$template){
   $this->get_view()->set('data',$data);
   return $this->get_view()->moduleRender('multiselect/view/'.$template);
 }



}
