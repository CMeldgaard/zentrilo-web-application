<?php
namespace Zentrilo\Modules\Multiselect;

class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function get(){
      $accountUUID = $_SESSION['accountUUID'];
      return $this->coredb
                  ->join('contacts_companies','contacts_persons.companyUUID=contacts_companies.companyUUID')
                  ->where("contacts_persons.accountUUID",$accountUUID)
                  ->orderby("fName","asc")
                  ->get("contacts_persons");
    }

    public function getData($UUID,$search){
      $accountUUID = $_SESSION['accountUUID'];
      return $this->coredb
                  ->join('contacts_companies','contacts_persons.companyUUID=contacts_companies.companyUUID')
                  ->where("contacts_persons.".$search,$UUID)
                  ->where("contacts_persons.accountUUID",$accountUUID)
                  ->get("contacts_persons");
    }

    public function getUUID($id){
      $data = $this->coredb
                  ->where("entityID",$id)
                  ->get("contacts_persons");
      return $data[0]["personUUID"];
    }

    public function getNames(){
      $accountUUID = $_SESSION['accountUUID'];
      return $this->coredb
                  ->where("accountUUID",$accountUUID)
                  ->orderby("fName","asc")
                  ->get("contacts_persons");
    }

    public function insert($data){
      $accountUUID = $_SESSION['accountUUID'];
      $contactData = Array ("accountUUID" => $accountUUID,
               "companyUUID" => $data["companyUUID"],
               "fName" => $data["fName"],
               "lName" => $data["lName"],
               "worktitle" => $data["worktitle"],
               "phone" => $data["phone"],
               "email" => $data["email"],
               "about" => $data["about"],
               "street" => $data["street"],
               "city" => $data["city"],
               "postal" => $data["postal"]
             );
      return $this->coredb->insert ('contacts_persons', $contactData);
    }

    public function update($data,$UUID){
      $accountUUID = $_SESSION['accountUUID'];
      $contactData = Array ("companyUUID" => $data["companyUUID"],
               "fName" => $data["fName"],
               "lName" => $data["lName"],
               "worktitle" => $data["worktitle"],
               "phone" => $data["phone"],
               "email" => $data["email"],
               "about" => $data["about"],
               "street" => $data["street"],
               "city" => $data["city"],
               "postal" => $data["postal"]
             );
      return $this->coredb
                  ->where('personUUID',$UUID)
                  ->where('accountUUID',$accountUUID)
                  ->update ('contacts_persons', $contactData);
    }

    public function delete($UUID){
      $accountUUID = $_SESSION['accountUUID'];
      $this->coredb
                  ->where('personUUID',$UUID)
                  ->where('accountUUID',$accountUUID)
                  ->delete ('contacts_persons');
    }
}
