<?php
namespace Zentrilo\Modules\Auth;
use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $Model;

  public function __construct() {
          // Load core controller functions
          parent::__construct(__NAMESPACE__.'\\Controller');
          // Load Module Model
          $this->Model = $this->load_model(__NAMESPACE__);
  }

  public function isLoggedIn()
  {
    $publicUrls = array('/signin','/signin/locked','/signin/post','/signup','/signup/post','/account/forgot-username','/account/forgot-password');
    $url = $_SERVER['REQUEST_URI'];

    if(!isset($_SESSION['userUUID']) and !in_array($url,$publicUrls)){
      header("Location: /signin");
      die();
    }
  }

  public function verifyCridentials($username,$password){
    $user = $this->Model->verifyUsername($username);
    if($user!=0){
      //Verify password
      $storedPassword = $user[0]["password"];
      $cryptCheck = $this->Model->verifyPassword($password,$storedPassword);
      if($cryptCheck){
        //Set user session and return succes
        $sessions = Autoloader::newLoad('Modules\\Sessions');
        $sessions->set('user',$user);
        return 1;
      }else{
        return 0;
      }
    }else{
      //Username wasnt found, returning false
      return $verifyUsername;
    }
  }

  public function AuthAccess($identifier,$type){
    //
  }

}
