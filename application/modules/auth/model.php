<?php
namespace Zentrilo\Modules\Auth;
use Autoloader;
class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function verifyUsername($username){
      $check = $this->coredb->where("userName",$username)->get("users");
      if(count($check)>0){
        return $check;
      }else{
        return 0;
      }
    }

    public function verifyPassword($password,$storedPassword){
      $Crypt = Autoloader::newLoad('Modules\\Crypt');
      $check = $Crypt->verifyHashedPassword($password,$storedPassword);
      return $check;
    }
}
