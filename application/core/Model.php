<?php
namespace Zentrilo\Core;
use MysqliDb;
class Model
{
  protected $db;
  protected $coredb;
  // Initiates DB connection via the MysqliDb Class
  public function __construct() {
      $this->coredb = new MysqliDb(COREDB_HOST, COREDB_USER, COREDB_PASSWORD, COREDB_NAME);
  }
}
