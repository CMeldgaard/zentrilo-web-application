<?php
// Load core files
require_once(COREPATH.'application.php');
require_once(COREPATH.'Controller.php');
require_once(COREPATH.'Model.php');
require_once(COREPATH.'MysqliDb.php');
require_once(COREPATH.'view.php');
require_once(COREPATH.'router.php');
require_once(COREPATH.'CSRF.php');
require_once(COREPATH.'functions.php');

//Set Aliases for easier use
class_alias('Zentrilo\\Core\\Autoloader', 'Autoloader');
class_alias('Zentrilo\\Core\\Application', 'Application');
class_alias('Zentrilo\\Core\\Router', 'Router');
class_alias('Zentrilo\\Core\\View', 'View');

// Default presenter to load — homepage requests are sent to this controller
define('DEFAULT_PRESENTER', 'dashboard');

// Defining Core Database connection information
define('COREDB_NAME', 'examserver39_core');
define('COREDB_USER', 'examserver39_zentrilo');
define('COREDB_PASSWORD', '(1qM+P2FutWR');
define('COREDB_HOST', 'examserver39.dk');
