<?php
namespace Zentrilo\Core;
class Controller extends Application
{

  protected $controller;
  protected $action;
  protected $models;
  protected $view;

  public function __construct($controller, $action = "index") {
      parent::__construct();
      $this->controller = $controller;
      $this->action = $action;
      $this->view = new View();
  }

  // Load model class
  protected function load_model($namespace) {
      $model = $namespace.'\\Model';
      // Convert namespace to full file path
      $namespace = str_replace('Zentrilo',"",$namespace);
      $filePath = $this->convertNamespace($namespace);

      //Load class
      require_once(strtolower(ROOT . $filePath));
      if(class_exists($model)) {
        return new $model();
      }
  }

  // Get the instance of the loaded model class
  protected function get_model($model) {
      if(is_object($this->models[$model])) {
          return $this->models[$model];
      } else {
          return false;
      }
  }

  protected function convertNamespace($namespace){
      $path = str_replace('\\', '/', $namespace) . '/model.php';
      return $path;
  }

  // Return view object
  protected function get_view() {
      return $this->view;
  }
}
