<?php
function filter(&$value) {
    $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}

Function humanTiming ($timeEnd, $timeStart)
{
    if ($timeStart<$timeEnd){
        $strStart = '2000-1-1 '.$timeStart;
        $strEnd = '2000-1-1 '.$timeEnd;
    }else{
        $strStart = '2000-1-1 '.$timeStart;
        $strEnd = '2000-1-2 '.$timeEnd;
    }

    $time = strtotime($strStart);
    $time = strtotime($strEnd)-$time; // to get the time since that moment
    $time = abs($time);
    if ($time<3600){
        return date('i:s',$time).' min';
    }else{
        return gmdate('H:i',$time).' timer';
    }
}

Function formatHours($time){
    $date1 = new DateTime("00:00:00");
    $date2 = new DateTime(date('d-m-Y G:i:s',$time));
    $diff = $date2->diff($date1);
    $days = $diff->format('%d');
    $daysHours = $days*24;
    $hours = (int)$diff->format('%H');
    $totalHours = $daysHours + $hours;
    return $totalHours.'t '.$diff->format('%I').'min';
}

Function decimalHours($time){
    $date1 = new DateTime("00:00:00");
    $date2 = new DateTime(date('d-m-Y G:i:s',$time));
    $diff = $date2->diff($date1);
    $days = $diff->format('%d');
    $daysHours = $days*24;
    $hours = $diff->format('%H') + $daysHours;
    $decimal = ((100/60)*($diff->format('%I')))/100;
    return $hours+$decimal;
}
