<?php
namespace Zentrilo\Core;

class Autoloader
{
    public static function load($namespace){
      // Convert namespace to full file path
      $filePath = strtolower(static::convertNamespace($namespace));
      //Load class
      if (!file_exists($filePath)){
        $filePath = strtolower(static::convertNamespace("presenter\\errors"));
        require_once($filePath);
      	return false;
      }
    	require_once($filePath);
      return $filePath;
    }

    public static function newLoad($namespace){
      static::load($namespace);
      $fullNamespace = 'Zentrilo\\'.$namespace.'\\Controller';
      $init = New $fullNamespace;
      return $init;
    }

    public function checkClass($namespace,$type){
      return "false";
    }

    protected static function convertNamespace($namespace){
        $path = ROOT. '/' . str_replace('\\', '/', $namespace) . '/controller.php';
        return $path;
    }
}
