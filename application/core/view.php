<?php
namespace Zentrilo\Core;
class View {

    protected $variables = array();

    public function __construct() { }

    // Set variables to be used in the view
    public function set($name,$value) {
        $this->variables[$name] = $value;
    }


    public function render($view_name) {
        extract($this->variables);
        if( file_exists(ROOT . DS . 'presenter' . DS . $view_name . '.php') ) {
            include (ROOT . DS . 'presenter' . DS . $view_name . '.php');
        } else {
            //Error handler
        }
    }

    public function returnRender($view_name) {
        extract($this->variables);
        if( file_exists(ROOT . DS . 'presenter' . DS . $view_name . '.php') ) {
          ob_start();
            include (ROOT . DS . 'presenter' . DS . $view_name . '.php');
          return ob_get_clean();
        } else {
            //Error handler
        }
    }

    public function moduleRender($view_name) {
        extract($this->variables);
        if( file_exists(ROOT . DS . 'modules' . DS . $view_name . '.php') ) {
          ob_start();
            include (ROOT . DS . 'modules' . DS . $view_name . '.php');
          return ob_get_clean();
        } else {
            //Error handler
        }
    }

    public function moduleOutRender($view_name) {
        extract($this->variables);
        if( file_exists(ROOT . DS . 'modules' . DS . $view_name . '.php') ) {
            include (ROOT . DS . 'modules' . DS . $view_name . '.php');
        } else {
            //Error handler
        }
    }
}
