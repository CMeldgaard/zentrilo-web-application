<?php
namespace Zentrilo\Core;

class Router {

    public function route($url) {
        // Split the URL into parts
        $url_array = array();
        $url_array = explode("/",$url);
        $url_count = count($url_array);

        // The first part of the URL is the presenter name
        $presenter = isset($url_array[0]) ? $url_array[0] : '';
        // if controller is empty, redirect to default controller
        if(empty($presenter)) { $presenter = DEFAULT_PRESENTER; }
        //shift array to remove controller
        array_shift($url_array);

        // The second part is the method name
        $method = isset($url_array[0]) ? $url_array[0] : '';
        if(empty($method)) { $method = 'index'; }
        array_shift($url_array);

        // The third part are the parameters
        $param = $url_array;

        //Load presenter controller
        $class = Autoloader::load('presenter\\'.$presenter);
        //Check if controller exists
        if(!$class){
          $presenter = "errors";
          $method = "show404";
        }
        //Initiate controller
        $controller = 'Zentrilo\\Presenter\\'.ucfirst($presenter).'\\Controller';
        $dispatch = new $controller($controller,$method);

        //Check if method exist, otherwise reinitiate the error presenter
        if(!method_exists($controller,$method)){
          $class = Autoloader::load('presenter\\Erros');
          $presenter = "errors";
          $method = "show500";
          $controller = 'Zentrilo\\Presenter\\'.ucfirst($presenter).'\\Controller';
          $dispatch = new $controller($controller,$method);
        }
        //Call Class Method with parameters
        call_user_func_array(array($dispatch,$method),$param);
    }
}
