<?php
namespace Zentrilo\Presenter\Dashboard;

class Controller extends \Zentrilo\Core\Controller
{
  public function __construct($controller,$method) {
          // Load core controller functions
          parent::__construct($controller, $method);
      }

  public function index() {
    // Render view
    $this->get_view()->set('title','Dashboard');
    $this->get_view()->render('dashboard/view/index');
  }
}
