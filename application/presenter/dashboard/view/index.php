<html>
<head>
<title>Kontaktpersoner - Zentrilo</title>
<?php require_once(ROOT."/presenter/shared/head.php");?>
</head>
<body class="contacts">
<!--Header-->
<?php require_once(ROOT."/presenter/shared/header.php");?>
<div>
  <?php require_once(ROOT."/presenter/shared/mainmenu.php");?>
  <div class="content-wrapper">
    <?php require_once(ROOT."/presenter/shared/overlay.php");?>
    <div class="uk-grid container noPad">
      <div class="uk-width-medium-4-4 uk-width-large-4-4 contactlist scroll">
        <div class="content">
          <div class="content-header">
            <h3><?php echo $title?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
