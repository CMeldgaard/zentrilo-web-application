<html>
<head>
<title>Opret konto | Zentrilo</title>
<?php require_once(ROOT."/presenter/shared/head.php");?>
</head>
<body class="signin uk-grid uk-height-1-1">
  <div class="uk-width-large-1-1 alignc">
    <div class="logomark">
    </div>
    <div class="centered" style="max-width:540px;">
      <div class="alignl">
        <h1 class="alignc mb30">Opret en konto i dag!</h1>
          <form action="/signup/post" id="signup-form" method="POST">
            <fieldset class="">
              <div class="line">
                  <label for="name" class="">Dit navn</label>
                  <input type="text" id="name" name="name" tabindex="1" autofocus="autofocus" value="">
              </div>
              <div class="line">
                  <label for="company" class="">Firmanavn</label>
                  <input type="text" id="company" name="company" tabindex="2" value="">
              </div>
              <div class="line">
                  <label for="email" class="">Email</label>
                  <input type="email" id="email" name="email" tabindex="3" value="">
              </div>
              <div class="line">
                  <label for="new_username" class="">Brugernavn</label>
                  <input type="text" name="username" tabindex="4" id="new_username" value="">
                  <div class="field-help"> Vælg et brugernavn der kun indeholder bogstaver og tal.
                  </div>
              </div>
              <div class="line mb30 uk-form-password">
                <label for="new_password" class="float-left ">Password</label>
                <input type="checkbox" name="show-password" id="show-password" class="hide" value="1">
                <label data-uk-form-password="{lblShow:'Vis', lblHide:'Skjul'}" for="show-password" title="Show Password" class="float-right uk-form-password-toggle show-password">Vis</label>
                <input type="password" name="password" tabindex="5" id="new_password" maxlength="51" value="">
              </div>
              <div class="line uk-grid">
                <div class="uk-width-medium-4-10">
                  <button id="create-account" type="submit" value="Create My Account" class="small-padding" tabindex="6">
                    Kom igang!
                  </button>
                </div>
                <div class="uk-width-medium-6-10">
                  <p class="inline-block small-meta">Ved at klikke her accepterer du Zentrilo's
                    <a href="http://zentrilo.dk/brugerbetingelser/" target="_blank">brugerbetingelser</a>.
                  </p>
                </div>
              </div>
            </fieldset>
          </form>
      </div>
    </div>
  </div>
</body>
</html>
