<?php
namespace Zentrilo\Presenter\Signup;
use Autoloader;
class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function signin($username,$password){
        //Load Auth module, sending username & password
        $Auth = Autoloader::newLoad('Modules\\Auth');
        $userCheck = $Auth->verifyCridentials($username,$password);
        return $userCheck;
    }

    public function getAccounts($UUID){
      $Accounts = Autoloader::newLoad('Modules\\Accounts');
      $conAccounts = $Accounts->connectedAccounts($UUID);
      return $conAccounts;
    }

    public function getName($UUID){
      $Accounts = Autoloader::newLoad('Modules\\Accounts');
      $name = $Accounts->accountName($UUID);
      return $name;
    }
}
