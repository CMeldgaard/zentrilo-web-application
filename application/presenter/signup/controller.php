<?php
namespace Zentrilo\Presenter\Signup;
use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $presenterModel;

  public function __construct($controller,$method) {
          // Load core controller functions
          parent::__construct($controller, $method);
          // Load presenter Model
          $this->presenterModel = $this->load_model(__NAMESPACE__);

      }

  public function index() {
      // Render view
      $this->get_view()->render('signup/view/index');
  }

  public function post() {
    //Verify username is uniq
    $username = $_POST["username"];
    $password = $_POST["password"];
    $users = Autoloader::newLoad('Modules\\Users');
    $uniq = $users->uniqUsername($username);
    if(empty($uniq)){
      //Create user
      $userEntityID = $users->createUser($_POST);

      //Create account
      $accounts = Autoloader::newLoad('Modules\\Accounts');
      $companyName = $_POST["company"];
      $accountEntityID = $accounts->createAccount($companyName);

      //Get UUID of user and account
      $userUUID = $users->getUUID($userEntityID);
      $accountUUID = $accounts->getUUID($accountEntityID);

      //Link user and account
      $accounts->linkAccount($userUUID,$accountUUID);

      //Login user
      $Auth = Autoloader::newLoad('Modules\\Auth');
      $Auth->verifyCridentials($username,$password);
      $_SESSION['accountUUID'] = $accountUUID;
      $_SESSION['accountName'] = $companyName;

      //Redirect to dashboard
      header("Location: /");
      die();
    }else{
      // Render view
      //Get gravatar for user
      $this->get_view()->render('signup/view/index');
    }
  }
}
