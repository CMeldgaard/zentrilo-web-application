<?php
namespace Zentrilo\Presenter\Contacts;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  public function __construct($controller,$method) {
          // Load core controller functions
          parent::__construct($controller, $method);
      }

  public function index() {
      // Render view
      $this->get_view()->set('title','Kontakter');
      $this->get_view()->render('contacts/view/index');
  }

  public function groups($UUID="",$referer="",$refererUUID="") {
    if($UUID!=""){
      //Set possible referer
      $this->get_view()->set('referer',$referer);
      $this->get_view()->set('refererUUID',$refererUUID);

      //Get contact info
      $contactGroups = Autoloader::newLoad('Modules\\ContactGroups');
      $groupData = $contactGroups->load($UUID,"contactGroupUUID");
      $groupArray = $groupData->getData();
      $this->get_view()->set('name',$groupArray[0]['groupName']);
      $this->get_view()->set('group',$groupArray[0]);

      //Build contact info header
      $headerText = $groupData->getHeader();
      $this->get_view()->set('headerText',$headerText);

      //Get linked persons
      $linkedPersons = $contactGroups->getLinkedPersons($UUID)->getData();
      $this->get_view()->set('linkedPersons',$linkedPersons);

      //Get linked companies
      $linkedCompanies = $contactGroups->getLinkedCompanies($UUID)->getData();
      $this->get_view()->set('linkedCompanies',$linkedCompanies);

      //Get history feed
      $history = Autoloader::newLoad('Modules\\History');
      $historyData = $history->getHistory($UUID);
      $historyLast = $historyData->lastUpdate();
      $historyList = $historyData->buildList();
      $this->get_view()->set('lastUpdate',$historyLast);
      $this->get_view()->set('history',$historyList);

      //Get task for group
      $_taskHelper = Autoloader::newLoad('Modules\\Tasks');
      $taskLoad = $_taskHelper->load($UUID);
      $tasklist = $taskLoad->getList();
      $taskCount = $taskLoad->taskCount();
      $this->get_view()->set('tasks',$tasklist);
      $this->get_view()->set('taskCount',$taskCount);

      // Render view
      $this->get_view()->moduleOutRender('contactgroups/view/view-group');

    }else{
      $this->get_view()->set('title','Kontaktgrupper');
      $multiselect = Autoloader::newLoad('Modules\\Multiselect');

      // Get created companies
      $companies = Autoloader::newLoad('Modules\\Companies');
      $createdCompanies = $companies->load()->getData();
      $companyOptions = $multiselect->getOptions($createdCompanies,"company");
      $this->get_view()->set('companyOptions',$companyOptions);

      //Get created persons
      $contacts = Autoloader::newLoad('Modules\\Contacts');
      $createdContacts = $contacts->load()->getData();
      $contactOptions = $multiselect->getOptions($createdContacts,"person");
      $this->get_view()->set('contactOptions',$contactOptions);

      //Get created groups
      $contactGroups = Autoloader::newLoad('Modules\\ContactGroups');
      $fullList = $contactGroups->load()->fullList();
      $this->get_view()->set('groupList',$fullList);

      // Render view
      $this->get_view()->render('contacts/view/list-groups');
    }


  }

  public function company($UUID="",$referer="",$refererUUID="") {
    if($UUID!=""){
      //Set possible referer
      $this->get_view()->set('referer',$referer);
      $this->get_view()->set('refererUUID',$refererUUID);

      //Get contact info
      $companies = Autoloader::newLoad('Modules\\Companies');
      $companyData = $companies->load($UUID);
      $companyArray = $companyData->getData();
      $this->get_view()->set('name',$companyArray[0]['companyName']);
      $this->get_view()->set('company',$companyArray[0]);

      $adresses = Autoloader::newLoad('Modules\\Addresses');
      $companyAdresses = $adresses->buildHTLM($companyArray);
      $this->get_view()->set('addresses',$companyAdresses);

      //Build contact info header
      $headerText = $companyData->getHeader();
      $this->get_view()->set('headerText',$headerText);

      //Get history feed
      $history = Autoloader::newLoad('Modules\\History');
      $historyData = $history->getHistory($UUID);
      $historyLast = $historyData->lastUpdate();
      $historyList = $historyData->buildList();
      $this->get_view()->set('lastUpdate',$historyLast);
      $this->get_view()->set('history',$historyList);

      //Get task for company
      $_taskHelper = Autoloader::newLoad('Modules\\Tasks');
      $taskLoad = $_taskHelper->load($UUID);
      $tasklist = $taskLoad->getList();
      $taskCount = $taskLoad->taskCount();
      $this->get_view()->set('tasks',$tasklist);
      $this->get_view()->set('taskCount',$taskCount);

      // Render view
      $this->get_view()->moduleOutRender('companies/view/view-company');

    }else{
      $this->get_view()->set('title','Virksomheder');

      // Get created companies
      $companies = Autoloader::newLoad('Modules\\Companies');
      $fullList = $companies->load()->fullList();
      $this->get_view()->set('companyList',$fullList);

      // Render view
      $this->get_view()->render('contacts/view/list-company');
    }


  }

  public function person($UUID="",$referer="",$refererUUID="") {
      //Determin if list or view
      if($UUID!=""){//View person
        $view = 'contacts/view/view-person';

        //Set possible referer
        $this->get_view()->set('referer',$referer);
        $this->get_view()->set('refererUUID',$refererUUID);

        //Get contact info
        $contact = Autoloader::newLoad('Modules\\Contacts');
        $contactData = $contact->load($UUID);
        $contactArray = $contactData->getData();
        $name = $contactArray[0]['fName'].' '.$contactArray[0]['lName'];
        $title = 'Kontaktpersoner / '.$name;
        $this->get_view()->set('name',$name);
        $this->get_view()->set('contact',$contactArray[0]);

        $adresses = Autoloader::newLoad('Modules\\Addresses');
        $contactAdresses = $adresses->buildHTLM($contactArray);
        $this->get_view()->set('addresses',$contactAdresses);

        //Build contact info header
        $headerText = $contactData->getHeader();
        $this->get_view()->set('headerText',$headerText);

        //Get history feed
        $history = Autoloader::newLoad('Modules\\History');
        $historyData = $history->getHistory($UUID);
        $historyLast = $historyData->lastUpdate();
        $historyList = $historyData->buildList();
        $this->get_view()->set('lastUpdate',$historyLast);
        $this->get_view()->set('history',$historyList);

        //Get task for person
        $_taskHelper = Autoloader::newLoad('Modules\\Tasks');
        $taskLoad = $_taskHelper->load($UUID);
        $tasklist = $taskLoad->getList();
        $taskCount = $taskLoad->taskCount();
        $this->get_view()->set('tasks',$tasklist);
        $this->get_view()->set('taskCount',$taskCount);

        // Render view
        $this->get_view()->set('title','Kontaktpersoner');
        $this->get_view()->moduleOutRender('contacts/view/view-person');

      }else{//Get list of persons
        // Get created companies
        $companies = Autoloader::newLoad('Modules\\Companies');
        $typeahead = Autoloader::newLoad('Modules\\TypeAhead');
        $companyData = $companies->load()->getData();
        $companyJSON = $typeahead->companyTypeAhead($companyData);
        $this->get_view()->set('companies',$companyJSON);

        //Get connected contacts & generate list HTML
        $Contacts = Autoloader::newLoad('Modules\\Contacts');
        $fullList = $Contacts->load()->fullList();
        $this->get_view()->set('contactList',$fullList);

        // Render view
        $this->get_view()->set('title','Kontaktpersoner');
        $this->get_view()->render('contacts/view/list-person');
      }
  }

  public function editperson($UUID){
    $contact = Autoloader::newLoad('Modules\\Contacts');
    $contactData = $contact->load($UUID);
    $contactArray = $contactData->getData();

    // Get created companies
    $companies = Autoloader::newLoad('Modules\\Companies');
    $companyData = $companies->load()->getData();
    $typeahead = Autoloader::newLoad('Modules\\Typeahead');
    $companyJSON = $typeahead->companyTypeAhead($companyData);
    $this->get_view()->set('companies',$companyJSON);

    $this->get_view()->set('contact',$contactArray[0]);
    $this->get_view()->moduleOutRender('contacts/view/edit-person');
  }

  public function editcompany($UUID){
    $companies = Autoloader::newLoad('Modules\\Companies');
    $companyData = $companies->load($UUID);
    $contactArray = $companyData->getData();

    $this->get_view()->set('company',$contactArray[0]);
    $this->get_view()->moduleOutRender('companies/view/edit-company');
  }

  public function updateperson($UUID){
    $data = $_POST;
    $companies = Autoloader::newLoad('Modules\\Companies');
    $contacts = Autoloader::newLoad('Modules\\Contacts');

    //Verify company exists, else create
    $company = $companies->load($data["companyName"],"companyName")->getData();
    if($company){
      //Get UUID and create user
      $companyUUID = $company[0]["companyUUID"];
      $data["companyUUID"] = $companyUUID;
      $create = $contacts->update($data,$UUID);
    }else{
      //Create company
      $companyID = $companies->quickCreate($data["companyName"]);
      $company = $companies->getCompany($companyID,"entityID")->getData();
      $companyUUID = $company[0]["companyUUID"];
      $data["companyUUID"] = $companyUUID;
      $create = $contacts->update($data,$UUID);
    }
  }

  public function updatecompany($UUID){
    $data = $_POST;
    $companies = Autoloader::newLoad('Modules\\Companies');
    $companies->update($data,$UUID);
  }

  public function saveperson(){
    $data = $_POST;
    //Verify company exists, else create
    $companies = Autoloader::newLoad('Modules\\Companies');
    $contacts = Autoloader::newLoad('Modules\\Contacts');
    $company = $companies->load($data["companyName"],"companyName")->getData();
    if($company){
      //Get UUID and create user
      $companyUUID = $company[0]["companyUUID"];
      $data["companyUUID"] = $companyUUID;
      $create = $contacts->create($data);
    }else{
      //Create company
      $companyID = $companies->quickCreate($data["companyName"]);
      $company = $companies->load($companyID,"entityID")->getData();
      $companyUUID = $company[0]["companyUUID"];
      $data["companyUUID"] = $companyUUID;
      $create = $contacts->create($data);
    }
    //Get connected contacts & generate list HTML
    $fullList = $contacts->load()->fullList();
    print $fullList;
  }

  public function savecompany(){
    $data = $_POST;
    $companies = Autoloader::newLoad('Modules\\Companies');
    $companies->create($data);

    //Get connected contacts & generate list HTML
    $fullList = $companies->load()->fullList();
    print $fullList;
  }

  public function savegroup(){
    $data = $_POST;
    $companies = Autoloader::newLoad('Modules\\Contactgroups');
    $companies->create($data);

    //Get connected groups & generate list HTML
    $fullList = $companies->load()->fullList();
    print $fullList;
  }

  public function deleteperson($uuid){
    $contacts = Autoloader::newLoad('Modules\\Contacts');
    $contacts->delete($uuid);
    //Get connected contacts & generate list HTML
    $fullList = $contacts->load()->fullList();
    print $fullList;
  }

  public function deletecompany($uuid){
    $companies = Autoloader::newLoad('Modules\\Companies');
    $companies->delete($uuid);
    //Get connected contacts & generate list HTML
    $fullList = $companies->load()->fullList();
    print $fullList;
  }
}
