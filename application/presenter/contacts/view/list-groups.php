<html>
<head>
<title>Kontaktpersoner - Zentrilo</title>
<?php require_once(ROOT."/presenter/shared/head.php");?>
</head>
<body class="contacts">
<!--Header-->
<?php require_once(ROOT."/presenter/shared/header.php");?>
<div>
  <?php require_once(ROOT."/presenter/shared/mainmenu.php");?>
  <div class="content-wrapper">
    <?php require_once(ROOT."/presenter/shared/overlay.php");?>
    <div class="guide">
      <img class="" src="/assets/images/get_data.svg">
      <h2 class="">
          <b>Klik</b> på en <b>gruppe</b> for at se data her
      </h2>
    </div>
    <div class="uk-grid container noPad">
      <div class="uk-width-medium-2-4 uk-width-large-2-4 contactlist scroll">
        <div class="content">
          <div class="content-header">
            <h3><?php echo $title?>
            </h3>
            <div class="header-tools">
              <div class="tool button active" id="sortUp" data-ul="contact-group">
                  <i class="fa fa-chevron-up" aria-hidden="true"></i> <span class="label">Sorter stigende</span>
              </div>
              <div class="tool button" id="sortDown" data-ul="contact-group">
                  <i class="fa fa-chevron-down" aria-hidden="true"></i> <span class="label">Sorter faldende</span>
              </div>
              <div class="tool float-right">
                  <input placeholder="Søg" class="filter-input" data-ul="contact-company" data-nano="contactlist">
              </div>
            </div>
          </div>
          <div class="content-button modal-toggle">
            Opret ny gruppe
          </div>
          <div class="block contactBlock full">
            <?php echo $groupList;?>
          </div>
        </div>
      </div>
    </div>
    <div class="content-slider scroll">
      SLIDER CONTENT
    </div>
    <?php require_once(ROOT."/modules/contactgroups/view/create-group-modal.php");?>
  </div>
</div>
</body>
</html>
