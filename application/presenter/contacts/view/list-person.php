<html>
<head>
<title>Kontaktpersoner - Zentrilo</title>
<?php require_once(ROOT."/presenter/shared/head.php");?>
</head>
<body class="contacts">
<!--Header-->
<?php require_once(ROOT."/presenter/shared/header.php");?>
<div>
  <?php require_once(ROOT."/presenter/shared/mainmenu.php");?>
  <div class="content-wrapper">
    <?php require_once(ROOT."/presenter/shared/overlay.php");?>
    <div class="guide">
      <img class="" src="/assets/images/get_data.svg">
      <h2 class="">
          <b>Klik</b> på en <b>person</b> for at se data her
      </h2>
    </div>
    <div class="uk-grid container noPad">
      <div class="uk-width-medium-2-4 uk-width-large-2-4 contactlist scroll">
        <div class="content">
          <div class="content-header">
            <h3><?php echo $title?>
              <a href="/contacts/company" class="fa fa-exchange" data-uk-tooltip="{pos:'right',animation:'true'}" title="Gå til virksomheder"></a>
            </h3>
            <div class="header-tools">
              <div class="tool button active" id="sortUp" data-ul="contact-person">
                  <i class="fa fa-chevron-up" aria-hidden="true"></i> <span class="label">Sorter stigende</span>
              </div>
              <div class="tool button" id="sortDown" data-ul="contact-person">
                  <i class="fa fa-chevron-down" aria-hidden="true"></i> <span class="label">Sorter faldende</span>
              </div>
              <div class="tool float-right">
                  <input placeholder="Søg" class="filter-input" data-ul="contact-person" data-nano="contactlist">
              </div>
            </div>
          </div>
          <div class="content-button modal-toggle">
            Tilføj ny kontaktperson
          </div>
          <div class="block contactBlock full">
            <?php echo $contactList;?>
          </div>
        </div>
      </div>
    </div>
    <div class="content-slider scroll">
      SLIDER CONTENT
    </div>
    <?php require_once(ROOT."/modules/contacts/view/create-person-modal.php");?>
  </div>
</div>
</body>
</html>
