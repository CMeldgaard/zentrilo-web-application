<?php
namespace Zentrilo\Presenter\Account;
use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $presenterModel;

  public function __construct($controller,$method) {
          // Load core controller functions
          parent::__construct($controller, $method);
          // Load presenter Model
          $this->presenterModel = $this->load_model(__NAMESPACE__);

      }

  public function index() {
      // Render view
      $this->get_view()->render('signin/view/index');
  }

  public function signout(){
    // Destroy all sessions from user.
    $sessions = Autoloader::newLoad('Modules\\Sessions');
    $sessions->destroy();
  }
}
