<html>
<head>
<title>Dashboard - Zentrilo</title>
<?php require_once("/presenter/shared/head.php");?>
</head>
<body class="">
<!--Header-->
<?php require_once("/presenter/shared/header.php");?>
<div>
  <?php require_once("/presenter/shared/mainmenu.php");?>
  <div class="content">
    <h3><?php echo $title?></h3>
    <?php require_once("/presenter/shared/pagebar.php");?>
  </div>
</div>
</body>
</html>
