<div class="modal-header">
    Opret opgave
</div>
<form class="uk-form small app-form edit-form" data-uuid="">
  <fieldset>
    <div class="uk-grid uk-margin uk-grid-right">
    <div class="uk-width-medium-1-1">
      <div class="line">
        <div class="field-wrapper">
          <label for="taskName" class="">Opgave</label>
          <input type="text" name="taskName" id="taskName">
        </div>
      </div>
    </div>
    <div class="uk-width-medium-1-2">
      <div class="line">
        <div class="field-wrapper">
          <label for="date" class="">Dato</label>
          <input type="text" name="date" id="date">
        </div>
      </div>
    </div>
    <div class="uk-width-medium-1-2">
      <div class="line">
        <div class="field-wrapper">
          <label for="time" class="">Klokken</label>
          <input type="text" name="time" id="time">
        </div>
      </div>
    </div>
    <div class="uk-width-medium-1-2">
      <div class="line">
        <div class="field-wrapper">
          <label for="label" class="">Kategori</label>
          <input class="companyName" type="text" name="label" id="label">
        </div>
      </div>
    </div>
    <div class="uk-width-medium-1-2">
      <div class="line">
        <div class="field-wrapper">
          <label for="assignedTo" class="">Ansvarlig</label>
          <input type="text" name="assignedTo" id="assignedTo">
        </div>
      </div>
    </div>
    <div class="uk-width-medium-2-2">
      <div class="line">
        <div class="field-wrapper">
          <label for="taskDetails" class="">Omkring</label>
          <textarea name="taskDetails" id="taskDetails" rows="3" ></textarea>
        </div>
      </div>
    </div>
  </div>
  </fieldset>
  <div class="formtools">
      <button class="cancel red cancel" data-uuid="" data-type="">
          Annuller <i class="fa fa-close"></i>
      </button>
      <button class="confirm saveTask">
          Gem <i class="fa fa-floppy-o"></i>
      </button>
  </div>
</form>
