<?php
namespace Zentrilo\Presenter\Tasks;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  public function __construct($controller,$method) {
          // Load core controller functions
          parent::__construct($controller, $method);
      }

  public function index() {
      // Render view
      $this->get_view()->set('title','Opgaver');
      $this->get_view()->render('tasks/view/index');
  }

  public function create($referenceUUID){
    $this->get_view()->set('UUID',$referenceUUID);
    // Render view
    $this->get_view()->render('tasks/view/create');
  }

  public function post($referenceUUID){
    $data = $_POST;
    print "Create task";
  }
}
