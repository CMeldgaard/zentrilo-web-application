<!--Begin header-->
<div class="page-header">
    <!--Begin logo-->
    <div class="page-logo">
        <a href="/">
            <img src="/assets/images/logomark.svg" class="img-logo" alt="logo">
        </a>
    </div>
    <!--End logo-->
    <!--Begin pagetop-->
    <div class="page-top">
      <div class="page-actions">
          <span class="icon-menu toggle-mainmenu"></span>
      </div>
      <div class="top-menu">
        <ul class="headernav">
          <li class="box notifications unread">
            <a href="#" title="Notifikationer">
              <i class="icon-feed" aria-hidden="true"></i>
            </a>
            <ul class="drop-down-container">
              <li>
                <div class="list">
                  <div class="nano notifications-scoller">
                    <div class="nano-content">
                      <ul>
                        <li class="notification unread"><a href="#">
                            <div class="image"><img src="/assets/images/avatar2.png" alt="Avatar"></div>
                            <div class="notification-info">
                              <div class="text"><span class="user-name">Jessica Caruso</span> accepted your invitation to join the team.</div><span class="date">2 min ago</span>
                            </div></a></li>
                        <li class="notification"><a href="#">
                            <div class="image"><img src="/assets/images/avatar2.png" alt="Avatar"></div>
                            <div class="notification-info">
                              <div class="text"><span class="user-name">Joel King</span> is now following you</div><span class="date">2 days ago</span>
                            </div></a></li>
                        <li class="notification"><a href="#">
                            <div class="image"><img src="/assets/images/avatar2.png" alt="Avatar"></div>
                            <div class="notification-info">
                              <div class="text"><span class="user-name">John Doe</span> is watching your main repository</div><span class="date">2 days ago</span>
                            </div></a></li>
                        <li class="notification"><a href="#">
                            <div class="image"><img src="/assets/images/avatar2.png" alt="Avatar"></div>
                            <div class="notification-info"><span class="text"><span class="user-name">Emily Carter</span> is now following you</span><span class="date">5 days ago</span></div></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="footer"> <a href="#">Se alle notifikationer</a></div>
              </li>
            </ul>
          </li>
          <li class="" id="user-menu-button">
              <img src="<?php echo $_SESSION['gravatar']?>">
          </li>
          <li class="submenu" id="user-menu-panel">
            <?php echo $_SESSION['userFname'][0]?>. <?php echo $_SESSION['userLname']?>
            <small><?php echo $_SESSION['accountName']?></small>
            <i class="fa fa-times user-menu-close" aria-hidden="true"></i>

              <div class="user-menu" id="user-menu">
                <ul class="">
                    <li class="">
                      <a href="" class="">
                        Min profil
                      </a>
                    </li>
                    <li class="">
                      <a href="" class="">
                        Plan & betaling
                      </a>
                    </li>
                    <li class="">
                      <a href="" class="">
                        Fakturaer
                      </a>
                    </li>
                    <li class="">
                        <a href="" class="">
                            Email notifikationer
                        </a>
                    </li>
                    <li class="">
                        <a href="" class="">
                            Skift password
                        </a>
                    </li>
                    <li class="">
                        <a href="" class="">
                            Skift konto
                        </a>
                    </li>
                </ul>
                <ul class="">
                    <li class="">
                        <a href="" class="">
                            Sikkerhedsindstillinger
                        </a>
                    </li>
                    <li class="">
                        <a href="" class="">
                            Email sharing indstilling
                        </a>
                    </li>
                    <li class="">
                        <a href="" class="">
                            Integrationer
                        </a>
                    </li>
                </ul>
              <ul class="">
                  <li class="">
                      <a href="" target="blank" class=" contactSupport">
                          Hjælpe Center
                      </a>
                  </li>

              </ul>
              <ul class="">
                <li class="">
                  <a href="/account/signout" class="">
                    Log ud
                  </a>
                </li>
              </ul>
              </div>
          </li>
        </ul>
      </div>
    </div>
    <!--End pagetop-->
</div>
<!--End header-->
