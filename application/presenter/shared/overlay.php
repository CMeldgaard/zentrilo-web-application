<div class="overlay">
  <div class="spinner">
    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
  </div>
</div>
<div class="confirmation-box">
  <h3>Bekræft</h3>
  <span class="text"></span>
  <div class="tools">
    <button type="button" class="cancel" id="cancel">
        Annuller
        <i class="fa fa-close"></i>
    </button>
    <button type="button" class="confirm" id="confirm" data-uuid="" data-type="">
        Slet
        <i class="fa fa-check"></i>
    </button>
  </div>
</div>
<div class="modal-box">
  <i class="fa fa-close close-modal"></i>
  <div class="content">
    MODAL CONTENT
  </div>
</div>
