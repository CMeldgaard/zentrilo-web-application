<div class="main-menu">
    <ul>
      <li class="nav-item">
        <a href="/" class="nav-link nav-toggle">
            <i class="icon-home"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>
    <li class="nav-item">
      <a href="#" class="nav-link nav-toggle">
          <i class="icon-people"></i>
          <span class="title">Kontakter</span>
      </a>
      <div class="submenu">
        <strong class="submenu-title">Kontakter</strong>
        <a href="#" class="action-close icon-close"></a>
        <ul>
          <li class="section">
            <ul>
              <li>
                <a href="/contacts/company" class="">
                  <span>Virksomheder</span>
                </a>
              </li>
              <li>
                <a href="/contacts/person" class="">
                  <span>Personer</span>
                </a>
              </li>
              <li>
                <a href="/contacts/groups" class="">
                  <span>Grupper</span>
                </a>
              </li>
            </ul>
          </li>
          <!--
          <li class="section">
            <strong class="submenu-group-title">
              <span>Værktøjer</span>
            </strong>
            <ul>
              <li>
                <a href="#" class="">
                  <span>Importer</span>
                </a>
              </li>
              <li>
                <a href="#" class="">
                  <span>Eksporter</span>
                </a>
              </li>
            </ul>
          </li>-->
        </ul>
      </div>
    </li>
  </ul>
    <div class="version">
      Zentrilo - v.0.1.0-alpha
    </div>
  </div>
