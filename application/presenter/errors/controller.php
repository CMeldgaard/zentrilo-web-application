<?php
namespace Zentrilo\Presenter\Errors;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  public function __construct($controller,$method) {
          // Load core controller functions
          parent::__construct($controller, $method);
      }

  public function show404() {
      // Render view
      $this->get_view()->set('title','Fejl 404');
      $this->get_view()->render('errors/view/index');
  }

  public function show500() {
      // Render view
      $this->get_view()->set('title','Fejl 500');
      $this->get_view()->render('errors/view/index');
  }
}
