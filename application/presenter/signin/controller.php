<?php
namespace Zentrilo\Presenter\Signin;
Use Autoloader;
class Controller extends \Zentrilo\Core\Controller
{
  protected $presenterModel;

  public function __construct($controller,$method) {
          // Load core controller functions
          parent::__construct($controller, $method);
          // Load presenter Model
          $this->presenterModel = $this->load_model(__NAMESPACE__);

      }

  public function index() {
      // Render view
      $this->get_view()->render('signin/view/signin');
  }

  public function post(){
    if($_POST){
      //Send username & password
      $username = $_POST["username"];
      $password = $_POST["password"];
      $Auth = Autoloader::newLoad('Modules\\Auth');
      $userCheck = $Auth->verifyCridentials($username,$password);
      if($userCheck==1){
        //Get connected accounts
        $Accounts = Autoloader::newLoad('Modules\\Accounts');
        $accounts = $Accounts->connectedAccounts($_SESSION['userUUID']);
        if(count($accounts)>1){
          // Render view and display possible accounts
          $this->get_view()->set('accounts', $accounts);
          $this->get_view()->set('fname',$_SESSION['userFname']);
          $this->get_view()->render('signin/view/select');
        }else{
          //If only one, call select function to set sessions etc then redirect
          $UUID = $accounts[0]["accountUUID"];
          $this->select($UUID);
        }
      }else{
        // Set view variables
          $this->get_view()->set('error', true);
          $this->get_view()->set('username', $username);
        // Render view
        $this->get_view()->render('signin/view/index');
      }
    }else{
      // Render view
      $this->get_view()->render('signin/view/index');
    }
  }
  public function select($UUID){
    $sessions = Autoloader::newLoad('Modules\\Sessions');
    $sessions->set('account',$UUID);
    header("Location: /");
    die();
  }
}
