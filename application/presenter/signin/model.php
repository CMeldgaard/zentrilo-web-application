<?php
namespace Zentrilo\Presenter\Signin;
use Autoloader;
class Model extends \Zentrilo\Core\Model {

    public function __construct() {
        // Load core model functions
        parent::__construct();
    }

    public function signin($username,$password){
        //Load Auth module, sending username & password
        $Auth = Autoloader::newLoad('Modules\\Auth');
        $userCheck = $Auth->verifyCridentials($username,$password);
        return $userCheck;
    }
}
