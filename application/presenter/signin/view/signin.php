<html>
<head>
<title>Log ind | Zentrilo</title>
<?php require_once(ROOT."/presenter/shared/head.php");?>
</head>
<body class="signin uk-grid uk-height-1-1">
  <div class="login uk-width-large-1-3 uk-height-1-1">
    <div class="logomark">
    </div>
    <form class="loginform uk-form" method="post" action="signin/post">
      <div class="headline">
        <h1>Log Ind</h1>
        <p>Brug for en Zentrilo konto?
          <a href="/signup" title="" onclick="ga('send', 'event', window.location.pathname, 'action', 'Opret konto fra login');">Opret konto i dag</a>.</p>
      </div>
      <fieldset>
        <?php
        if(isset($error)): ?>
        <div class="notification-container">
          <div class="notification error">
            <div class="title">Fejl</div>
            <div class="message">Det indtastede brugernavn eller password er forkert</div>
          </div>
        </div>
      <?php endif?>
        <div class="line login-field">
          <div class="field-wrapper">
            <label for="username" class="">Brugernavn</label>
            <input type="text" name="username" id="username" tabindex="1" autofocus="autofocus" autocorrect="off" autocapitalize="off" value="<?php echo isset($username) ? $username : "";?>">
          </div>
        </div>
        <div class="line mb30 uk-form-password">
          <label for="password" class="float-left ">Password</label>
          <input type="checkbox" name="show-password" id="show-password" class="hide" value="1">
          <label data-uk-form-password="{lblShow:'Vis', lblHide:'Skjul'}" for="show-password" title="Show Password" class="float-right uk-form-password-toggle show-password">Vis</label>
          <input type="password" name="password" tabindex="2" id="password" maxlength="51" value="">
        </div>
        <div class="line">
          <button type="submit" value="log ind" class="full-width mb30" tabindex="3">Log ind</button>
          <div class="alignc">
            <label for="stay-signed-in">
              <div class="inline-block stay-signed-in">
                <input class="" name="stay-signed-in" type="checkbox" tabindex="0" value="1" id="stay-signed-in">
              </div>Husk mig i 2 uger
            </label>
          </div>
        </div>
        <div class="alignc">
          <p class="inline-block">
            <a href="/account/forgot-username">Glemt brugernavn?</a>
          </p>
          &nbsp;&nbsp;|&nbsp;&nbsp;
          <p class="inline-block">
            <a href="/account/forgot-password">Glemt password?</a>
          </p>
        </div>
      </fieldset>
    </form>
  </div>
  <div class="loginimage uk-width-large-2-3 uk-height-1-1">
  </div>
</body>
</html>
