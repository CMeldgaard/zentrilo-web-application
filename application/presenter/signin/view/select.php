<html>
<head>
<title>Vælg en konto | Zentrilo</title>
<?php require_once(ROOT."/presenter/shared/head.php");?>
</head>
<body class="signin uk-grid uk-height-1-1">
  <div class="uk-width-large-1-1 alignc">
    <div class="logomark">
    </div>
    <div class="centered" style="max-width:500px;">
      <div class="alignl">
        <h1 class="alignc mb30">Velkommen tilbage, <?php echo $fname?>!</h1>
        <p>Vælg en konto eller <a href="/signup">lav en ny</a></p>
        <ul class="accountSelectBox">
          <?php
          $i=1;
          foreach ($accounts as $account):
          ?>
          <li id="<?php echo $i?>" class="account" data-account-name="Geek Media" title="Geek Media account" style="position:relative">
            <a href="/signin/select/<?php echo $account["accountUUID"]?>" tabindex="2" class="h4"><?php echo $account["accountName"]?>
              <span class="icon-arrow-right" role="presentation"></span>
            </a>
          </li>
        <?php
        $i++;
        endforeach
        ?>
        </ul>
      </div>
    </div>
  </div>
</body>
</html>
