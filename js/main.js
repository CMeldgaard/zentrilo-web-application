$(document).ready(function(){
	//Prevent form submit with enter
	$(document).on("keypress", ":input:not(textarea)", function(event) {
    return event.keyCode != 13;
	});

	//Toggle create modal
	$( ".modal-toggle" ).click(function(event) {
		event.preventDefault();
		$(".add-modal").toggleClass("open");
		$('.add-modal form')[0].reset();
	});

	//Close modal
		$( ".close-modal" ).click(function() {
			$(".modal-box").removeClass("show");
			$(".overlay").removeClass("show");
		});

	//Save modal
	$( ".modal-save" ).click(function(event) {
		event.preventDefault();
		var requrl = $(this).attr("href");
		$(".add-modal.open .modal-toggle").fadeOut();
		$(".modal-save i").removeClass('fa-floppy-o').addClass('fa-spinner fa-pulse fa-fw');
		var formData = $(".add-modal.open form").serialize();
		$.ajax({
		  method: "POST",
		  url: requrl,
		  data: formData
		}).done(function(html) {
				$(".contact-person").replaceWith(html);
				$(".contact-company").replaceWith(html);
				$(".contact-group").replaceWith(html);
				$(".add-modal").removeClass("open");
				$(".add-modal .modal-toggle").fadeIn();
				$(".modal-save i").removeClass('fa-spinner fa-pulse fa-fw').addClass('fa-floppy-o');
		  });
	});

	//Fetch contact data
	$('.contactBlock').on( "click", ".getContact", function() {
		var userUUID = $(this).data("uuid");
		$(".getContact.selected").removeClass("selected");
		getContact(userUUID);
		$(this).toggleClass("selected");
	});

	//Fetch company data
	$('.contactBlock').on( "click", ".getCompany", function() {
		var companyUUID = $(this).data("uuid");
		$(".getCompany.selected").removeClass("selected");
		getCompany(companyUUID);
		$(this).toggleClass("selected");
	});

	//Fetch group data
	$('.contactBlock').on( "click", ".getGroup", function() {
		var groupUUID = $(this).data("uuid");
		$(".getGroup.selected").removeClass("selected");
		getGroup(groupUUID);
		$(this).toggleClass("selected");
	});

	//Toggle back to referer
	$('.content-slider').on( "click", ".referer-toggle", function() {
		$(".content-slider").removeClass("open");
		var uuid = $(this).data('uuid');
		var type = $(this).data('type');
		if(type=="person"){
			getContact(uuid);
		}
		if(type=="group"){
			getGroup(uuid);
		}
		if(type=="company"){
			getCompany(uuid);
		}
	});

	//Edit opened contact
	$('.content-slider').on( "click", ".editContact", function() {
		$(".content-slider").removeClass("open");
		var uuid = $(this).data('uuid');
		var type = $(this).data('type');
		if(type==="person"){
			editContact(uuid);
		}
		if(type==="company"){
			editCompany(uuid);
		}
	});

	//Cancel edit
	$('.content-slider').on( "click", ".cancel-task", function(event) {
		event.preventDefault();
		var uuid = $(this).data('uuid');
		var type = $(this).data('type');
		if(type==="person"){
			getContact(uuid);
		}
		if(type==="company"){
			getCompany(uuid);
		}
	});

	//Save edited person/company
	$('.content-slider').on( "submit", ".edit-form", function(event) {
		event.preventDefault();
		var formData = $(".content-slider form").serialize();
		var uuid = $(this).data('uuid');
		var type = $(this).data('type');
		if(type==="person"){
			updateContact(uuid,formData);
		}
		if(type==="company"){
			updateCompany(uuid,formData);
		}
	});

	//Confirm contact delete
	$('.content-slider').on( "click", ".deleteContact", function() {
		var uuid = $(this).data('uuid');
		var type = $(this).data('type');
		$(".overlay").addClass("show");
		$(".overlay .spinner").hide();
		$(".confirmation-box").addClass("show");
		$(".confirmation-box #confirm").data("uuid",uuid);
		$(".confirmation-box #confirm").data("type",type);
		if(type==="person"){
			$(".confirmation-box .text").html("Er du sikker på at du vil slette denne kontakt?");
		}
		if(type==="company"){
			$(".confirmation-box .text").html("Er du sikker på at du vil slette denne virksomhed? Dette vil også slette tilknyttede kontaktpersoner!");
		}

	});

	//Toggle content-slider
	$('.content-slider').on( "click", ".content-slider-toggle", function() {
		$(".content-slider").removeClass("open");
		$(".getContact.selected").removeClass("selected");
		$(".getCompany.selected").removeClass("selected");
		$(".getGroup.selected").removeClass("selected");
	});

});
