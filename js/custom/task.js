function newTask(referenceUUID){
	$(".overlay").addClass("show");
	var requrl = "/tasks/create"+'/'+referenceUUID;
	$(".content-slider").removeClass("open");
	$.ajax({
		method: "GET",
		url: requrl
	}).done(function(html) {
    $(".content-slider").html(html).toggleClass("open");
    $(".overlay").removeClass("show");
		});
}

function createTask(referenceUUID,formData){
  $(".overlay").addClass("show");
	var requrl = "/tasks/post"+'/'+referenceUUID;
	$(".content-slider").removeClass("open");
	$.ajax({
		method: "POST",
		data: formData,
		url: requrl
	}).done(function() {
    var callBack = localStorage.getItem('callBack');
    var refUUID = localStorage.getItem('referenceUUID');
    window[callBack](refUUID);
	});
}

$(document).ready(function(){
  /*//New task
  $('.content-slider').on( "click", ".newTask", function() {
    var referenceUUID = $(this).data("uuid");
    var callBack = $(this).data("callback");
    localStorage.setItem('callBack', callBack);
    localStorage.setItem('referenceUUID', referenceUUID);
    newTask(referenceUUID);
  });

  //Create task
  $('.content-slider').on( "click", ".saveTask", function() {
    var formData = $(".content-slider form").serialize();
    createTask(formData);
  });*/
});
