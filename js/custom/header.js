$(document).ready(function(){

  //Notifications toggle
  $( ".notifications>a" ).click(function() {
    $(".notifications").toggleClass("open");
  });
  $(document).mouseup(function (e)
  {
      var container = $(".notifications");
      if (!container.is(e.target) // if the target of the click isn't the container...
          && container.has(e.target).length === 0) // ... nor a descendant of the container
      {
          $(".notifications").removeClass("open");
      }
  });

  //User menu toggle
  $( "#user-menu-button" ).click(function() {
    $(".headernav").toggleClass("open");
  });
  $( ".user-menu-close" ).click(function() {
    $(".headernav").toggleClass("open");
  });

});
