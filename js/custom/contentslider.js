function updateContentSlider(html){
	$(".content-slider").html(html).toggleClass("open");
	$(".overlay").removeClass("show");
}

$(document).ready(function(){
  //Cancel
  $('.content-slider').on( "click", ".cancel", function() {
    var callBack = localStorage.getItem('callBack');
    var refUUID = localStorage.getItem('referenceUUID');
    window[callBack](refUUID);
  });
});
