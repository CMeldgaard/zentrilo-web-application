$(document).ready(function(){

  //Sort UL list
	// accending sort
	function asc_sort(a, b){
		 return ($(b).data('filter').toLowerCase()) < ($(a).data('filter').toLowerCase()) ? 1 : -1;
	}
	// decending sort
	function dec_sort(a, b){
		 return ($(b).data('filter').toLowerCase()) > ($(a).data('filter').toLowerCase()) ? 1 : -1;
	}

	$("#sortUp").click(function() {
		$(".tool.active").toggleClass("active");
		var ulClass = '.'+$(this).data("ul");
		$(ulClass+' li').sort(asc_sort).appendTo(ulClass);
		$(this).toggleClass("active");
	});

	$("#sortDown").click(function() {
		$(".tool.active").toggleClass("active");
		var ulClass = '.'+$(this).data("ul");
		$(ulClass+' li').sort(dec_sort).appendTo(ulClass);
		$(this).toggleClass("active");
	});

	//Filter UL list
	$('.filter-input').keyup(function(){
		var list = '.'+$(this).data("ul");
		var nano = '.'+$(this).data("nano");
   	var valThis = $(this).val().toLowerCase();
    if(valThis == ""){
        $(list+' > li').show();
				setTimeout(function(){ $(nano).nanoScroller(); }, 100);
    } else {
        $(list+' > li').each(function(){
            var text = $(this).data("filter").toLowerCase();
            (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
        });
				setTimeout(function(){ $(nano).nanoScroller(); }, 100);
   };
});

});
