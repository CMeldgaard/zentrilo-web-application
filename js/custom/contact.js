function getContact(personUUID, referer, referertype){
	$(".overlay").addClass("show");
	var requrl = "/contacts/person"+'/'+personUUID;
	if(referer !== undefined){
		requrl = requrl+'/'+referer+'/'+referertype;
	}
	$(".content-slider").removeClass("open");
	$.ajax({
		method: "GET",
		url: requrl
	}).done(function(html) {
			updateContentSlider(html);
		});
}

function editContact(personUUID){
	$(".overlay").addClass("show");
	var requrl = "/contacts/editperson"+'/'+personUUID;
	$(".content-slider").removeClass("open");
	$.ajax({
		method: "GET",
		url: requrl
	}).done(function(html) {
			updateContentSlider(html);
		});

}

function updateContact(personUUID,formData){
	$(".overlay").addClass("show");
	var requrl = "/contacts/updateperson"+'/'+personUUID;
	$(".content-slider").removeClass("open");
	$.ajax({
		method: "POST",
		data: formData,
		url: requrl
	}).done(function() {
			getContact(personUUID)
		});
}

function deleteContact(personUUID){
	$(".overlay").addClass("show");
	var requrl = "/contacts/deleteperson"+'/'+personUUID;
	$(".content-slider").removeClass("open");
	$.ajax({
		method: "GET",
		url: requrl
	}).done(function(html) {
			$(".contact-person").replaceWith(html);
			$(".overlay").removeClass("show");
		});
}
