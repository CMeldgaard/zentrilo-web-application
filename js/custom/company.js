//Load company into contentslider
function getCompany(companyUUID, referer, referertype){
	$(".overlay").addClass("show");
	var requrl = "/contacts/company"+'/'+companyUUID;
	if(referer !== undefined){
		requrl = requrl+'/'+referer+'/'+referertype;
	}
	$(".getCompany.selected").removeClass("selected");
	$(".content-slider").removeClass("open");
	$(this).toggleClass("selected");
	$.ajax({
		method: "GET",
		url: requrl
	}).done(function(html) {
			updateContentSlider(html);
		});
}

//Edit company
function editCompany(companyUUID){
	$(".overlay").addClass("show");
	var requrl = "/contacts/editcompany"+'/'+companyUUID;
	$(".content-slider").removeClass("open");
	$.ajax({
		method: "GET",
		url: requrl
	}).done(function(html) {
			updateContentSlider(html);
		});

}

//Update company
function updateCompany(companyUUID,formData){
	$(".overlay").addClass("show");
	var requrl = "/contacts/updatecompany"+'/'+companyUUID;
	$(".content-slider").removeClass("open");
	$.ajax({
		method: "POST",
		data: formData,
		url: requrl
	}).done(function() {
			getCompany(companyUUID)
		});
}

//Delete company
function deleteCompany(companyUUID){
	$(".overlay").addClass("show");
	var requrl = "/contacts/deletecompany"+'/'+companyUUID;
	$(".content-slider").removeClass("open");
	$.ajax({
		method: "GET",
		url: requrl
	}).done(function(html) {
			$(".contact-company").replaceWith(html);
			$(".overlay").removeClass("show");
		});
}
