//Load company into contentslider
function getGroup(companyUUID, referer, referertype){
	$(".overlay").addClass("show");
	var requrl = "/contacts/groups"+'/'+companyUUID;
	if(referer !== undefined){
		requrl = requrl+'/'+referer+'/'+referertype;
	}
	$(".getCompany.selected").removeClass("selected");
	$(".content-slider").removeClass("open");
	$(this).toggleClass("selected");
	$.ajax({
		method: "GET",
		url: requrl
	}).done(function(html) {
			updateContentSlider(html);
		});
}
