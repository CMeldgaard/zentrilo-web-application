$(document).ready(function(){
	$( ".confirmation-box #cancel" ).click(function() {
		$(".overlay").removeClass("show");
		$(".confirmation-box").removeClass("show");
		setTimeout(function(){ $(".overlay .spinner").show(); }, 1000);
	});
	$( ".confirmation-box #confirm" ).click(function() {
		var uuid = $(this).data('uuid');
		var type = $(this).data('type');
		$(".confirmation-box").removeClass("show");
		$(".overlay .spinner").show();
		$(".content-slider").removeClass("open");
		if(type==="person"){
			deleteContact(uuid);
		}
		if(type==="company"){
			deleteCompany(uuid);
		}
	});
});
