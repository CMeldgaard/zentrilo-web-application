$(document).ready(function(){
	//Toggle main menu on mobile
	$( ".toggle-mainmenu" ).click(function(event) {
		$(".main-menu").toggleClass("open");
	});

	//Main menu controller
	$('.main-menu .nav-item>a').click(function() {
		//Close any open menu
    	$('.main-menu .nav-item.open').removeClass('open');
		//Open selected menu
    	$(this).parent('li').addClass('open');
	});
	$('.main-menu .nav-item .action-close').click(function() {
		//Close any open menu
    	$('.main-menu .nav-item.open').removeClass('open');
	});
});
