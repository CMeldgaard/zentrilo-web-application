var gulp          = require('gulp');
var	sass          = require('gulp-sass');
var sourcemaps    = require('gulp-sourcemaps');
var	postcss       = require('gulp-postcss');
var	autoprefixer  = require('autoprefixer');
var mqpacker      = require('css-mqpacker');
var csswring      = require('csswring');
var concat        = require('gulp-concat-sourcemap');
var uglify        = require('gulp-uglify');
var imagemin      = require('gulp-imagemin');
var notify        = require('gulp-notify');
var browserSync   = require('browser-sync').create();
var ftp          = require('vinyl-ftp');
var reload        = browserSync.reload; // For manual browser reload.
var runSequence = require('run-sequence');

gulp.task('styles', function(){
	var postcssProcessors = [
		autoprefixer(),
		mqpacker,
		csswring
	];

	return gulp.src('./sass/main.sass')
		.pipe(sourcemaps.init())
		.pipe(sass({style: 'compressed'}).on('error', sass.logError))
		.pipe(postcss(postcssProcessors))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./application/assets/css'))
    .pipe(browserSync.stream());
});

gulp.task('styles-build', function(){
	var postcssProcessors = [
		autoprefixer(),
		mqpacker,
		csswring
	];

	return gulp.src('./sass/main.sass')
		.pipe(sass({style: 'compressed'}).on('error', sass.logError))
		.pipe(postcss(postcssProcessors))
		.pipe(gulp.dest('./application/assets/css'))
    .pipe(browserSync.stream());
});

gulp.task('scripts', function(){
	return gulp.src([
		'./js/thirdparty/*.js',
		'./js/custom/*.js',
		'./js/main.js',
		'./bower_components/uikit/js/core/core.js',
		'./bower_components/uikit/js/core/grid.js',
		'./bower_components/uikit/js/core/dropdown.js',
		'./bower_components/uikit/js/core/tab.js',
		'./bower_components/uikit/js/core/switcher.js',
		'./bower_components/uikit/js/components/form-password.js',
		'./bower_components/uikit/js/components/tooltip.js',
		'./bower_components/typeahead.js/dist/typeahead.jquery.min.js'])
		.pipe(sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(uglify())
		//.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./application/assets/js'))
    .pipe(browserSync.stream());
});

gulp.task('images', function(){
	return gulp.src('./img/*')
		.pipe(imagemin())
		.pipe(gulp.dest('./application/assets/images'))
    .pipe(browserSync.stream());
});

gulp.task('serve', ['styles','scripts'], function(gulpCallback) {
  browserSync.init({
    // serve out of app/
    proxy   : "http://dev.zentrilo.dk"
  }, function callback() {
    // (server is now up)

    // watch html and reload browsers when it changes
    gulp.watch('application/**/*.php', browserSync.reload);
		gulp.watch('sass/**/*.sass', ['styles']);
		gulp.watch('js/**/*.js', ['scripts']);
		gulp.watch('img/**/*', ['images']);

    // notify gulp that this task is done
    gulpCallback();
  });
});

gulp.task('default', ['serve']);
// ### Vinyl FTP
var conn = ftp.create( {
	host:     'ftp.examserver39.dk',
	user:     'examserver39',
	password: 'cNgt$.ST6BGf',
	parallel: 10
});
var globs = [
	'application/**/*',
	'.htaccess'
];
gulp.task('deploy', function() {
  // using base = '.' will transfer everything to /public_html correctly
  // turn off buffering in gulp.src for best performance
  return gulp.src( globs, { base: 'application', buffer: false } )
    .pipe( conn.newer( '/development.examserver39.dk' ) ) // only upload newer files
    .pipe( conn.dest( '/development.examserver39.dk' ) );
});

gulp.task('deploy-live', function() {
  // using base = '.' will transfer everything to /public_html correctly
  // turn off buffering in gulp.src for best performance
  return gulp.src( globs, { base: 'application', buffer: false } )
    .pipe( conn.newer( '/app.examserver39.dk' ) ) // only upload newer files
    .pipe( conn.dest( '/app.examserver39.dk' ) );
});

// ### Build
gulp.task('build', function(callback) {
  runSequence('styles-build',
              'scripts',
              'images',
              'deploy',
              callback);
});

// ### Build to live server
gulp.task('build-live', function(callback) {
  runSequence('styles-build',
              'scripts',
              'images',
              'deploy-live',
              callback);
});
